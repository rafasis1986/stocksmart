# -*- coding: utf-8 -*-
'''
Created on 20/5/2015

@author: rtorres
'''
from rauth import OAuth1Service
import webbrowser


def getSession():
    # Create a session
    # Use actual consumer secret and key in place of 'foo' and 'bar'
    consumer_key = '59fcf5137c2c801febe2d3ce760c6e3d',
    service = OAuth1Service(
              name = 'etrade',
              consumer_key = '59fcf5137c2c801febe2d3ce760c6e3d',
              consumer_secret = 'dfe771a025f3806f75a9698405d7aba2',
              request_token_url = 'https://etws.etrade.com/oauth/request_token',
              access_token_url = 'https://etws.etrade.com/oauth/access_token',
              authorize_url = 'https://us.etrade.com/e/t/etws/authorize?key={}&token={}',
              base_url = 'https://etws.etrade.com')

    # Get request token and secret
    #===============================================================================================
    # response = service.get_request_token(params ={'oauth_callback': 'oob', 
    #                                'format': 'json'})
    #===============================================================================================
    oauth_token, oauth_token_secret = service.get_request_token(params = 
                                  {'oauth_callback': 'oob', 
                                   'format': 'json'})
    print('oauth_token', oauth_token, 'oauth_token_secret', oauth_token_secret)
    auth_url = service.authorize_url.format(consumer_key, oauth_token)

    # Get verifier (direct input in console, still working on callback)
    webbrowser.open(auth_url)
    verifier = input('Please input the verifier: ')

    return service.get_auth_session(oauth_token, oauth_token_secret, params = {'oauth_verifier': verifier})

# Create a session
session = getSession()

# After authenticating a session, use sandbox urls
url = 'https://etwssandbox.etrade.com/accounts/sandbox/rest/accountlist.json'

resp = session.get(url, params = {'format': 'json'})

print(resp)