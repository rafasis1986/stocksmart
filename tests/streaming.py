# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from ast import *
from pprint import pprint
import unicodedata

import decimal
from _ast import AST
from threading import Thread

# Go to http://apps.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key = ""
consumer_secret = ""

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token = ""
access_token_secret = ""

consumer_key = "5v2dPa5MG0G6CNkooo8quRKJX"
consumer_secret = "d14HxhwSVESI9guPmO3HUP3D1SylKZ02dk440ElEePBLiXB7OY"
access_token = "56209575-NvO53saDGnfoQh5J3Ump0X3uP3GWjX1kKNRsBnaLE"
access_token_secret = "9bhHlzAQxm7fXQw3tljfcgCLV7DX8WzP6WKzQHANmoiXG"

def literal_eval(node_or_string):
    """
    Safely evaluate an expression node or a string containing a Python
    expression.  The string or node provided may only consist of the following
    Python literal structures: strings, numbers, tuples, lists, dicts, booleans,
    and None.
    """
    _safe_names = {'None': None, 'True': True, 'False': False, 'null': None, 'true': True, 'false':False}
    if isinstance(node_or_string, basestring):
        node_or_string = parse(node_or_string, mode='eval')
    if isinstance(node_or_string, Expression):
        node_or_string = node_or_string.body
    def _convert(node):
        if isinstance(node, Str):
            return node.s
        elif isinstance(node, Num):
            return node.n
        elif isinstance(node, Tuple):
            return tuple(map(_convert, node.elts))
        elif isinstance(node, List):
            return list(map(_convert, node.elts))
        elif isinstance(node, Dict):
            return dict((_convert(k), _convert(v)) for k, v
                        in zip(node.keys, node.values))
        elif isinstance(node, Name):
            if node.id in _safe_names:
                return _safe_names[node.id]
        elif isinstance(node, BinOp) and \
             isinstance(node.op, (Add, Sub)) and \
             isinstance(node.right, Num) and \
             isinstance(node.right.n, complex) and \
             isinstance(node.left, Num) and \
             isinstance(node.left.n, (int, long, float)):
            left = node.left.n
            right = node.right.n
            if isinstance(node.op, Add):
                return left + right
            else:
                return left - right
        raise ValueError('malformed string')
    return _convert(node_or_string)


class StdOutListener(StreamListener):
    """ A listener handles tweets are the received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    criteria = None
    
    def __init__(self, criteria=None):
        super(StdOutListener, self).__init__()
        self.criteria
        
    def on_data(self, data):
        dicci = literal_eval(data)
        print(dicci)
        return True

    def on_error(self, status):
        print(status)

def runStreamListener(name, auth):
        streamListener = StdOutListener()
        stream = Stream(auth, streamListener)
        stream.filter(track=[name])

if __name__ == '__main__':
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)


    
    tracks=['baseball,''basket', 'venezuela', 'usa', 'colombia', 'germany', 'apple', 'football']
    for item in tracks:
        t = Thread(target=runStreamListener, args=(item, auth))
        t.start()
        
        
    

