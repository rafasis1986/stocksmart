# -*- coding: utf-8 -*-
'''
Created on 07/04/2015

@author: rtorres
'''
import sys

import requests

import json


WS_HOST = 'http://localhost:8000/'

def create_user(username):
    values = {'username': username, 'password': username}
    r = requests.post(WS_HOST + 'users/',
                      data=values, verify=False)
    return json.loads(r.content)

def get_token(username):
    values = {'username': username, 'password': username}
    r = requests.post(WS_HOST + 'login/',
                      data=values, verify=False)
    return json.loads(r.content)

def affiliate_stock(username, stock_name):
    token = get_token(username)
    values = {'name': stock_name}
    headers = {'Authorization': 'JWT ' + token.get('token')}
    r = requests.post(WS_HOST + 'users/stocks/',
                      data=values, headers=headers, verify=False)
    return json.loads(r.content)

def get_stocks(username):
    token = get_token(username)
    headers = {'Authorization': 'JWT ' + token.get('token')}
    r = requests.get(WS_HOST + 'users/stocks/',
                    headers=headers, verify=False)
    return json.loads(r.content)

def disaffiliate_stock(username, stock_id):
    token = get_token(username)
    headers = {'Authorization': 'JWT ' + token.get('token')}
    r = requests.delete(WS_HOST + 'users/stocks/' + stock_id + '/',
                    headers=headers, verify=False)
    return r.content

def main(argv):
    username = argv[0]
    funcion = argv[1].upper()
    if funcion == 'C':
        print create_user(username)
    elif funcion == 'T':
        print get_token(username)
    elif funcion == 'A':
        stock_name = argv[2].upper()
        print affiliate_stock(username, stock_name)
    elif funcion == 'D':
        stock_id = argv[2]
        print disaffiliate_stock(username, stock_id)
    elif funcion == 'L':
        print get_stocks(username)
    else:
        print "Invalid action \n"
        print "Please enter #username together one of the next options: \n"
        print "- Create user:  C"
        print "- get Token user:  T"
        print "- Affiliate stock:  A"
        print "- Disaffiliate stock:  D"
        print "- query the List stocks affiliate:  L"


if __name__ == "__main__":
    if len(sys.argv) > 1 :
        main(sys.argv[1:])
    else:
        print "missed args"
