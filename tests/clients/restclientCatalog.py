# -*- coding: utf-8 -*-
'''
Created on Aug 14, 2015

@author: rtorres
'''
import requests
import json, csv
from stock_app.models import StockCatalogModel
import time

print("start")


with open('nyse.csv', 'rb') as f:
    reader = csv.reader(f)
    your_list = list(reader)
errorList = list()
for item in your_list:
    try:
        stock = StockCatalogModel()
        stock.symbol = item[0]
        stock.name = item[1]
        stock.sector = item[5]
        stock.industry = item[6]
        stock.dateCreated = long(time.time())
        stock.save()
        print('succes ', stock.symbol)
    except Exception as e:
        print("error symbol", item[0], type(e))

print("end")