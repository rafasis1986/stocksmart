# -*- coding: utf-8 -*-
'''
Created on Aug 14, 2015

@author: rtorres
'''
import requests
import json, csv

minIndex = 1
maxIndex = 200
headers = {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJvb3QiLCJvcmlnX2lhdCI6MTQzOTU1NzUwMSwidXNlcl9pZCI6MSwiZW1haWwiOiJyb290QHBpaWN0dS5jb20iLCJleHAiOjE0NDIxNDk1MDF9.8GdyGqLOjnoQABzEFMJxtcSbry5s40ysQr9-MCaLG6w',
           'content-type': 'application/json'}
url = 'http://api.stockbe.at/v1/users/me/portfolios/BWY7WWeh/stocks'


with open('nasdaq.csv', 'rb') as f:
    reader = csv.reader(f)
    your_list = list(reader)
print(len(your_list))
for item in your_list[minIndex:maxIndex]:
    data = {"stocks": item[0], "sector": item[5], "industry": item[6]}
    response = requests.post(url, data=json.dumps(data), headers=headers)
    jsonData = json.loads(response.content)
    errorList = list()
    errorDict = dict()
    if jsonData.get('error'):
        errorDict.update({'symbol': item[0]})
        errorDict.update({'error': jsonData.get('error')})
        errorDict.update({'code': jsonData.get('code')})
        errorDict.update({'message': jsonData.get('message')})
        errorList.append(errorDict)
        errorList.append(errorDict)

writer = csv.writer(open('nasdaqError.csv', 'a'))
# writer.writerow(['symbol', 'error', 'code', 'message'])
for error in errorList:
    items = list()
    for key, value in error.items():
        items.append(value)
    writer.writerow(items)


