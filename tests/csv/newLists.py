import csv
from stock_app.models.stockCatalogModel import StockCatalogModel
from googlefinance import getQuotes

with open('ETFList.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    exFile = open('exclude.csv', 'w')
    inFile = open('include.csv', 'w')
    exCsv = csv.writer(exFile)
    inCsv = csv.writer(inFile)
    header = True
    for row in spamreader:
        if header:
            exCsv.writerow(row)
            inCsv.writerow(row)
            header = False
        else:
            symbol = str(row[0]).strip().upper()[1:-1]
            try:
                getQuotes(symbol)
                inCsv.writerow(row)
            except StockCatalogModel.DoesNotExist:
                exCsv.writerow(row)
    exCsv.close()
    inCsv.close()
