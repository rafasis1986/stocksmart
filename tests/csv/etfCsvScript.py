import csv
from stock_app.models.stockCatalogModel import StockCatalogModel

with open('include.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    header = True
    for row in spamreader:
        if header:
            header = False
        else:
            symbol =  str(row[0]).strip().upper() 
            name = str(row[1]).strip()
            for rowName in row[2:]:
                name += ', ' + str(rowName).strip()
            try:
                StockCatalogModel.objects.get(symbol=symbol)
            except StockCatalogModel.DoesNotExist:
                StockCatalogModel.objects.get_or_create(symbol=symbol, name=name)

