import ystockquote
import time

#===================================================================================================
# while True:
#     print(ystockquote.get_price('GOOGL'))
#     time.sleep(2)
#===================================================================================================


import urllib2
import json
import time

class GoogleFinanceAPI:
    def __init__(self):
        self.prefix = "http://finance.google.com/finance/info?client=ig&q="
    
    def get(self,symbol,exchange):
        url = self.prefix+"%s:%s"%(exchange,symbol)
        u = urllib2.urlopen(url)
        content = u.read()
        
        obj = json.loads(content[3:])
        return obj[0]
    
    def getSymbols(self,symbols):
        stringSymbols = ','.join([symbol for symbol in symbols])
        url = self.prefix+"%s"%(stringSymbols)
        u = urllib2.urlopen(url)
        content = u.read()
        
        obj = json.loads(content[3:])
        return obj
        
if __name__ == "__main__":
    c = GoogleFinanceAPI()
    
    while 1:
        #quote = c.get("MSFT","NASDAQ")
        quote = c.getSymbols(["MSFT","AAPL", "GOOGL"])
        for item in quote:
            print(item['t'],item['l_cur'])
        time.sleep(0.5)
