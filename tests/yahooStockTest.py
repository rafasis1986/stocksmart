from stockretriever import get_current_info, get_index_summary
from stock_app.models import Stock, IndexModel

#lista = ['HP', 'GM', 'AA', 'YELP', 'MFT', 'DSNY', '^IXIC', '^GSPC', '^DJI', 'AAPL', 'FEYE', 'TSLA', 'PBR', 'FB', 'DBA', 'PHYS', 'TETHF', 'ETSY', 'YHOO', 'GLD', 'DELL', 'AMZN', 'PEGI', 'VSLR', 'NEP', 'AMBA', 'FIATY', 'EBAY', 'FCEL', 'NFLX', 'GOOG', 'BP', 'GPRO', 'SSYS', 'SPY', 'BABA', 'SCTY', 'ZNGA', 'SLB', 'A', 'BOX', 'XOM', 'MSFT', 'KO', 'F', 'TWTR', 'HTM', 'BIDU', 'GOOGL', 'IMPV']
lista = ['HP', 'BRK.B']
actu = 0
print(actu)
sumary = None
try:
    responseStock = get_current_info(lista, columnsToRetrieve=['Symbol', 'Name', 'Open', 'FiftydayMovingAverage', 'TwoHundreddayMovingAverage',
                             'PreviousClose', 'YearHigh', 'YearLow', 'LastTradePriceOnly'])
    for item in responseStock:
        name = item['Name']
        print(name)
        closePrice = -100 if item['LastTradePriceOnly'] is None else float(item['LastTradePriceOnly'])
        symbol = item['Symbol']
        if symbol[0] == '^':
            symbol = IndexModel.objects.get(yahooSymbol=symbol).stock.symbol
        #Stock.objects.filter(symbol=symbol).update(closePrice=closePrice)
        actu += 1
except Exception as e:
    print e.message
print(sumary)
print(actu)
