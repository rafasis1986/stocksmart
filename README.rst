===========================
piictu-finance-back project
===========================

A project template for Django 1.8

To use this project follow these steps:

#. Create your working environment
#. Install Django
#. Install additional dependencies



Working Environment
===================

You have several options in setting up your working environment.  We recommend
using virtualenv to separate the dependencies of your project from your system's
python environment.  If on Linux or Mac OS X, you can also use virtualenvwrapper to help manage multiple virtualenvs across different projects.

Virtualenv Only
---------------

First, make sure you are using virtualenv (http://www.virtualenv.org). Once
that's installed, create your virtualenv::

    $ virtualenv finance

You will also need to ensure that the virtualenv has the project directory
added to the path. Adding the project directory will allow `django-admin.py` to
be able to change settings using the `--settings` flag.

Virtualenv with virtualenvwrapper
------------------------------------

In Linux and Mac OSX, you can install virtualenvwrapper (http://virtualenvwrapper.readthedocs.org/en/latest/),
which will take care of managing your virtual environments and adding the
project path to the `site-directory` for you::

    $ mkdir finance
    $ mkvirtualenv -a finance finance-dev
    $ cd icecream && add2virtualenv `pwd`

Using virtualenvwrapper with Windows
----------------------------------------

There is a special version of virtualenvwrapper for use with Windows (https://pypi.python.org/pypi/virtualenvwrapper-win).::

    > mkdir finance
    > mkvirtualenv finance-dev
    > add2virtualenv finance


Installing Django
=================

To install Django in the new virtual environment, run the following command::

    $ pip install django>=1.8


Installation of Dependencies
=============================

Depending on where you are installing dependencies:

In development::

    $ pip install -r requirements/local.txt

For production::

    $ pip install -r requirements.txt

*note:

-   We install production requirements this way because many Platforms as a
Services expect a requirements.txt file in the root of projects.

-   Every time that file requirementes/base.txt or some file related to our work 
environment has changes, we need run again the sentence "pip install -r " to adding the new 
dependencies to our environment.
*


Sync Database Project
========================

We need to have previusly created database a database and user, following the 
detailed specifications on the files "finance-back/finance/settings/config_local.py" if
we are in a development enviroment or as may be specified in 
"finance-back/finance/settings/config_production.py" if we are in a production enviroment.

Having validated that the system credentials to connect to the DBMS. We proceed 
to execute the following statement::

    $ python manage.py migrate --no-initial-data

And then::

    $ python manage.py migrate

By running twice the sentence guarantee that no errors occur dependencies 
relationships with objects not yet created

*note: We can adjust the local and production configuration file at our convenience, 
without affecting the enviroment of the other people invvolved in the project.*
