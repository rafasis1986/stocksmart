# -*- coding: utf-8 -*-
'''
Created on 7/4/2015

@author: rtorres
'''
from __builtin__ import Exception
from threading import Thread
import json
import time

from django.contrib.auth.models import User, Group
from django.db import transaction
from django.http.request import QueryDict
from django.views.decorators.csrf import csrf_exempt
from phonenumbers.phonenumberutil import NumberParseException
from rest_condition import And
from rest_framework import serializers, status
from rest_framework.exceptions import ParseError
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_409_CONFLICT, HTTP_200_OK
from rest_framework.views import APIView
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.views import ObtainJSONWebToken
import phonenumbers

from config.utils.errorResponses import LogicalErrorResponse, TechnichalErrorResponse
from config.utils.permissions import IsPostRequest, IsAppRequest, IsGetRequest
from login.models import ActivationCodeModel
from login.serializers import UserSerializer
from login.utils.constants import GROUP_PUBLIC, STATUS_EXPIRED, STATUS_ACTIVATED
from login.utils.errorMessages import MSG_PHONE_USER_ERROR, MSG_SENDING_CODE_SMS
from login.utils.errorResponses import UserExistsError, UsernameMissedError, CreateUserError, \
    PasswordMissedError, EmailMissedError, LoginMissedError, PhoneMissedError, PhoneFormatError
from login.utils.exceptions import MissedUsernameError, MissedPasswordError, MissedEmailError, \
    MissedPhoneError, MissedCodeError, InvalidCodeError, ExpiredCodeError, \
    InvalidPhoneError, UserExistException
from login.utils.functions import user_exist, jwt_response_payload_handler, sendSmsCode
from login.utils.functions.phones import formatPhoneToE164
from login.utils.requestTags import PASSWORD_TAG, EMAIL_TAG, USERNAME_TAG, PHONE_TAG, CODE_TAG, \
    MESSAGE_TAG


class UsersView(ListModelMixin, RetrieveModelMixin, GenericAPIView):
    """
    @precondition: To use the method 'GET' you must be authenticated when one Admin account user.

    @function <b>GET</b>: returns the list of all users account from the system.
    """
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [And(IsAdminUser, IsGetRequest)]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class UserDetailView(RetrieveAPIView):
    """
    @precondition: To use the method 'GET' you must be authenticated when JWT token Authorization on your header request

    @function <b>GET</b>: returns the details user
    """

    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)


class UserLoginView(ObtainJSONWebToken):
    """
    @precondition: To use the method 'POST' you must be authenticated when JWT token Authorization on your header request

    @function <b>POST</b>: returns the details user
    """
    permission_classes = [And(IsPostRequest, IsAppRequest)]

    def post(self, request):
        auxRequest = dict()
        serializer = None
        response = None
        try:
            if type(request.data) is QueryDict:
                for item in request.data:
                    item = json.loads(str(item))
                    auxRequest.update({PASSWORD_TAG: item.get(PASSWORD_TAG)})
                    if item.get(EMAIL_TAG) is not None:
                        auxRequest.update({USERNAME_TAG: item.get(EMAIL_TAG)})
                    else:
                        auxRequest.update({USERNAME_TAG: item.get(USERNAME_TAG)})
                    break
                serializer = self.serializer_class(data=auxRequest)
            else:
                if request.data.get(EMAIL_TAG) is not None:
                    request.DATA.update({'username': request.data.get(EMAIL_TAG)})
                serializer = self.serializer_class(data=request.DATA)
            if serializer.is_valid():
                user = serializer.object.get('user') or request.user
                if user.is_active is False:
                    raise Exception()
                token = serializer.object.get('token')
                response_data = jwt_response_payload_handler(token, user, request)
                response = Response(response_data)
        except serializers.ValidationError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_401_UNAUTHORIZED)
        if response is None:
            response = Response(LoginMissedError().__dict__, status=HTTP_400_BAD_REQUEST)
        return response


class CreateUserView(CreateAPIView):
    """
    @precondition: To use the method 'POST' you must be adding the App-Authorization to your header request

    @function <b>POST</b>: this create one user account (not authentication required).<br/>
    <ul>
        <li>@param <strong>username</strong>: is the username to a new usser account.</li>
        <li>@param <strong>password</strong>: it is not mandatory (By default is the same username).</li>
        <li>@param <strong>email</strong>: it is not mandatory (By default is 'dumy@piictu.com').</li>
    </ul>
    """
    model = User
    serializer_class = UserSerializer
    permission_classes = [And(IsPostRequest, IsAppRequest)]

    @csrf_exempt
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        response = None
        user = None
        try:
            username = request.data.get(EMAIL_TAG)
            password = request.data.get(PASSWORD_TAG)
            phone = request.data.get(PHONE_TAG)
            email = request.data.get(EMAIL_TAG)
            if password is None:
                raise MissedPasswordError()
            elif email is None or username is None:
                raise MissedEmailError()
            elif username is None:
                raise MissedUsernameError()
            elif phone is None:
                raise MissedPhoneError()
            else:
                if user_exist(username):
                    userAux = User.objects.get(username=username)
                    if userAux.is_active:
                        raise UserExistException()
                    userAux.delete()
                phone = formatPhoneToE164(phone)
                if not phone[1:].isdigit() or len(phone) < 5:
                    raise InvalidPhoneError()
                else:
                    phonenumbers.parse(phone, None)
                user = User()
                user.username = username
                user.set_password(password)
                user.email = email
                user.phone = phone
                user.is_active = False
                user.save()
                code = ActivationCodeModel()
                code.user = user
                code.save()
                Thread(target=sendSmsCode, args=(code.code, user.phone.as_e164)).start()
                response = Response({MESSAGE_TAG: MSG_SENDING_CODE_SMS}, status=HTTP_200_OK)
        except NumberParseException as e:
            response = Response(PhoneFormatError(e.message).__dict__, status=HTTP_409_CONFLICT)
        except UserExistException:
            response = Response(UserExistsError().__dict__, status=HTTP_400_BAD_REQUEST)
        except InvalidPhoneError as e:
            response = Response(PhoneFormatError(e.message).__dict__, status=HTTP_400_BAD_REQUEST)
        except MissedUsernameError:
            response = Response(UsernameMissedError().__dict__, status=HTTP_400_BAD_REQUEST)
        except MissedPasswordError:
            response = Response(PasswordMissedError().__dict__, status=HTTP_400_BAD_REQUEST)
        except MissedEmailError:
            response = Response(EmailMissedError().__dict__, status=HTTP_400_BAD_REQUEST)
        except MissedPhoneError as e:
            response = Response(PhoneMissedError(e.message).__dict__, status=HTTP_400_BAD_REQUEST)
        except ParseError as e:
            response = Response(LogicalErrorResponse(e.detail).__dict__, status=HTTP_409_CONFLICT)
        except Exception as e:
            if PHONE_TAG in e.message:
                response = Response(CreateUserError(message=MSG_PHONE_USER_ERROR).__dict__, status=HTTP_409_CONFLICT)
            else:
                response = Response(CreateUserError().__dict__, status=HTTP_400_BAD_REQUEST)
        return response


class ActiveUserView(APIView):
    """
    View to activate user
    """
    permission_classes = [IsAppRequest]

    @csrf_exempt
    @transaction.atomic
    def post(self, request):
        response = None
        user = None
        try:
            phone = request.data.get(PHONE_TAG)
            code = request.data.get(CODE_TAG)
            if phone is None:
                raise MissedPhoneError
            elif code is None:
                raise MissedCodeError()
            else:
                actiCode = None
                phone = formatPhoneToE164(phone)
                if not phone[1:].isdigit() or len(phone) < 5:
                    raise InvalidPhoneError()
                else:
                    phonenumbers.parse(phone, None)
                try:
                    actiCode = ActivationCodeModel.objects.get(code=code)
                except ActivationCodeModel.DoesNotExist:
                    raise InvalidCodeError()
                if actiCode.user.phone.as_e164 == str(phone):
                    if actiCode.status is STATUS_EXPIRED:
                        raise ExpiredCodeError()
                    else:
                        actiCode.status = STATUS_ACTIVATED
                        user = actiCode.user
                        if user.is_active:
                            raise InvalidCodeError()
                        user.is_active = True
                        user.save()
                        Group.objects.get(name=GROUP_PUBLIC).user_set.add(user)
                        user.save()
                        payload = jwt_payload_handler(user)
                        if api_settings.JWT_ALLOW_REFRESH:
                            payload['orig_iat'] = long(time.time())
                        token = jwt_encode_handler(payload)
                        response_data = jwt_response_payload_handler(token, user, request)
                        response = Response(response_data)
                else:
                    raise InvalidPhoneError()
        except NumberParseException as e:
            response = Response(PhoneFormatError(e.message).__dict__, status=HTTP_409_CONFLICT)
        except InvalidCodeError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_401_UNAUTHORIZED)
        except ExpiredCodeError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_401_UNAUTHORIZED)
        except InvalidPhoneError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except ParseError as e:
            response = Response(LogicalErrorResponse(e.detail).__dict__, status=HTTP_409_CONFLICT)
        except Exception as e:
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_401_UNAUTHORIZED)
        return response
