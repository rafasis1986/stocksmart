# -*- coding: utf-8 -*-
'''
Created on 9/7/2015

@author: rtorres
'''

APP_PREFIX = 'login'

GROUP_PRIVATE = "PRIVATE"
GROUP_PUBLIC = 'EVERYONE'

STATUS_ACTIVATED = 'activated'
STATUS_ERROR = 'error'
STATUS_EXPIRED = 'expired'
STATUS_SENT = 'sent'
STATUS_WAITING = 'waiting'
