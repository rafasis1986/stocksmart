# -*- coding: utf-8 -*-
'''
Created on 07/04/2015

@author: rtorres
'''
import time

from django.conf import settings
from django.contrib.auth.models import User
from django.db import transaction
import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException
from twilio.rest.client import TwilioRestClient
from twilio.rest.exceptions import TwilioRestException

from login.models import ActivationCodeModel
from login.serializers import UserSerializer
from login.utils.constants import STATUS_SENT, STATUS_WAITING, STATUS_ERROR
from login.utils.errorMessages import MSG_SEND_CODE_SMS, MSG_PHONE_FORMAT_ERROR, \
    MSG_CODE_STATUS_ERROR
from login.utils.exceptions import InvalidCodeError


def raiser(exception):
    raise exception


def user_exist(username):
    '''
    @param username: user login
    @type  username: string

    @return: True in case that the user exists
    '''
    try:
        User.objects.get(username=username)
    except User.DoesNotExist:
        return False
    return True


def jwt_response_payload_handler(token, user=None, request=None):
    """
    Returns the response data for both the login and refresh views.
    Override to return a custom response such as including the
    serialized representation of the User.
    """
    return {
        'token': token,
        'user': UserSerializer(user).data
    }


@transaction.atomic
def sendSmsCode(code, phone):
    codeObject = None
    try:
        codeObject = ActivationCodeModel.objects.get(code=code)
        codeObject.dateSent = long(time.time())
        if codeObject.status != STATUS_WAITING:
            raise InvalidCodeError()
        client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
        phonenumbers.parse(phone, None)
        toPhone = phone
        messageText = MSG_SEND_CODE_SMS % (settings.PROJECT_NAME, codeObject.code)
        if settings.TWILIO_ENVIROMENT_TEST:
            messageText = messageText + " phone: " + toPhone
            toPhone = settings.TWILIO_PHONE_TESTING
        message = client.messages.create(
            to=toPhone,
            from_=settings.TWILIO_PHONE_NUMBER,
            body=messageText,
        )
        codeObject.message = message.sid
        codeObject.status = STATUS_SENT
    except ActivationCodeModel.DoesNotExist:
        codeObject = None
    except NumberParseException:
        codeObject.status = STATUS_ERROR
        codeObject.message = MSG_PHONE_FORMAT_ERROR + ' phone: ' + phone
    except TwilioRestException as e:
        codeObject.status = STATUS_ERROR
        codeObject.message = e.msg
    except InvalidCodeError:
        codeObject.message = MSG_CODE_STATUS_ERROR
    except Exception as e:
        if type(codeObject) is ActivationCodeModel:
            codeObject.status = STATUS_ERROR
            codeObject.message = e.message
        else:
            codeObject = None
    finally:
        if type(codeObject) is ActivationCodeModel:
            codeObject.save()
