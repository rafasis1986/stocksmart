# -*- coding: utf-8 -*-
'''
Created on Sep 10, 2015

@author: rtorres
'''
from login.utils.exceptions import MissedPhoneError, InvalidPhoneError


def formatPhoneToE164(phone=''):
    flag = True if phone[0] == '+' else False
    phone = phone.replace('(', '').replace(')', '').replace('-', '').replace('+', '').\
        replace(' ', '').replace('#', '').replace('_', '').replace('.', '')
    while len(phone) > 0:
        if phone[0] == '0':
            phone = phone[1:]
            flag = False
        else:
            break
    if not phone.isdigit() and len(phone) < 5:
        raise InvalidPhoneError()
    if flag:
        phone = '+' + phone
    else:
        phone = '+1' + phone
    return str(phone)
