# -*- coding: utf-8 -*-
"""
Created on 9/7/2015

@author: rtorres
"""

MSG_CREATE_USER_ERROR = "create user exception"
MSG_CODE_STATUS_ERROR = "the code has a different state than expected"
MSG_DONT_USER_EXISTS_ERROR = "user do not exist"
MSG_DONT_HAVE_PERMISSION = "user do not have permmision"
MSG_LOGIN_ERROR = "please check your user account credentials"
MSG_MISSED_EMAIL_ERROR = "you forgot send the email address"
MSG_MISSED_PASSWORD_ERROR = "you forgot send the password"
MSG_MISSED_PHONE_ERROR = "you forgot send the phone number account"
MSG_MISSED_USERNAME_ERROR = "you forgot send the username account"
MSG_PHONE_USER_ERROR = "your phone number has been added previously with other user account"
MSG_PHONE_FORMAT_ERROR = "your phone number not have E164 format"
MSG_SENDING_CODE_SMS = "You will receive in the next minutes a text message with the user account activation code"
MSG_SEND_CODE_SMS = "%s verification code %s "
MSG_SEND_CODE_SMS_TEST = "%s verification code %s to phone number %$"
MSG_USER_EXISTS_ERROR = "username already exists"
