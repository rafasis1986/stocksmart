# -*- coding: utf-8 -*-
'''
Created on 14/5/2015

@author: rtorres
'''
from login.utils.errorMessages import MSG_USER_EXISTS_ERROR


class DontHavePermissionError(Exception):

    def __init__(self):
        self.message = "You do not have permission to execute this operation."


class MissedEmailError(Exception):

    def __init__(self):
        self.message = "You forgotten enter a email address."


class MissedPasswordError(Exception):

    def __init__(self):
        self.message = "You forgotten enter a password."


class MissedUsernameError(Exception):

    def __init__(self):
        self.message = "You forgotten enter a username account."


class MissedLoginError(Exception):

    def __init__(self):
        self.message = "You need check your user account and password."


class MissedPhoneError(Exception):

    def __init__(self):
        self.message = "You forgotten enter a phone number."


class MissedCodeError(Exception):

    def __init__(self):
        self.message = "You forgotten enter the code number to activate the user account."


class InvalidUserError(Exception):

    def __init__(self):
        self.message = "email user invalid."


class InvalidPhoneError(Exception):

    def __init__(self):
        self.message = "phone user invalid."


class InvalidCodeError(Exception):

    def __init__(self):
        self.message = "activation code invalid."


class ExpiredCodeError(Exception):

    def __init__(self):
        self.message = "your activation code has expired."


class UserExistException(Exception):

    def __init__(self):
        self.message = MSG_USER_EXISTS_ERROR
