# -*- coding: utf-8 -*-
'''
Created on 9/7/2015

@author: rtorres
'''
from config.utils.errorResponses import LogicalErrorResponse, TechnichalErrorResponse
from login.utils.errorMessages import MSG_USER_EXISTS_ERROR, MSG_CREATE_USER_ERROR, \
    MSG_MISSED_USERNAME_ERROR, MSG_LOGIN_ERROR, MSG_DONT_USER_EXISTS_ERROR, MSG_DONT_HAVE_PERMISSION,\
    MSG_MISSED_PHONE_ERROR, MSG_PHONE_FORMAT_ERROR, MSG_MISSED_EMAIL_ERROR


class UserExistsError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(UserExistsError, self).__init__(MSG_USER_EXISTS_ERROR if message is None else message)
        self.code = 2001
        self.error = self.__class__.__name__


class CreateUserError(TechnichalErrorResponse):

    def __init__(self, message=None):
        super(CreateUserError, self).__init__(MSG_CREATE_USER_ERROR if message is None else message)
        self.code = 1001
        self.error = self.__class__.__name__


class UsernameMissedError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(UsernameMissedError, self).__init__(MSG_MISSED_USERNAME_ERROR if message is None else message)
        self.code = 2002
        self.error = self.__class__.__name__


class EmailMissedError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(EmailMissedError, self).__init__(MSG_MISSED_EMAIL_ERROR if message is None else message)
        self.code = 2003
        self.error = self.__class__.__name__


class PasswordMissedError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(PasswordMissedError, self).__init__(MSG_MISSED_USERNAME_ERROR if message is None else message)
        self.code = 2004
        self.error = self.__class__.__name__


class LoginMissedError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(LoginMissedError, self).__init__(MSG_LOGIN_ERROR if message is None else message)
        self.code = 2005
        self.error = self.__class__.__name__


class UserDontExistError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(UserDontExistError, self).__init__(MSG_DONT_USER_EXISTS_ERROR if message is None else message)
        self.code = 2006
        self.error = self.__class__.__name__


class UserDonHavePermissionError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(UserDonHavePermissionError, self).__init__(MSG_DONT_HAVE_PERMISSION if message is None else message)
        self.code = 2007
        self.error = self.__class__.__name__


class PhoneMissedError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(PhoneMissedError, self).__init__(MSG_MISSED_PHONE_ERROR if message is None else message)
        self.code = 2012
        self.error = self.__class__.__name__


class PhoneFormatError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(PhoneFormatError, self).__init__(MSG_PHONE_FORMAT_ERROR if message is None else message)
        self.code = 2013
        self.error = self.__class__.__name__
