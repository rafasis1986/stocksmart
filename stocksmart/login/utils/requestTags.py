# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''

CODE_TAG = 'code'
EMAIL_TAG = 'email'
MESSAGE_TAG = 'message'
PASSWORD_TAG = 'password'
PHONE_TAG = 'phone'
USERNAME_TAG = 'username'
