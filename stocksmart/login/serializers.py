# -*- coding: utf-8 -*-
'''
Created on 9/7/2015

@author: rtorres
'''
from django.contrib.auth.models import User, Group
from rest_framework import serializers


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('name')


class UserSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField('userGroups')

    def userGroups(self, user):
        return [item['name'] for item in user.groups.all().values('name').order_by('name')]

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'phone', 'groups')


class UserSignUpSerializer(UserSerializer):

    class Meta(UserSerializer.Meta):
        fields = ('username', 'email', 'phone')
