# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('login', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivationCodeModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=6, db_index=True)),
                ('dateCreated', models.PositiveIntegerField()),
                ('dateSent', models.PositiveIntegerField(default=0, db_index=True)),
                ('status', models.CharField(default=b'waiting', max_length=16)),
                ('message', models.TextField(null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'login_acti_code',
                'verbose_name': 'activation code ',
                'verbose_name_plural': 'activation codes',
            },
        ),
        migrations.RemoveField(
            model_name='usermessage',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserMessage',
        ),
    ]
