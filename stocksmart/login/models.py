# -*- coding: utf-8 -*-
'''
Created on Jul 24, 2015

@author: rtorres
'''
import time

from django.contrib.auth.models import User
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from login.utils.constants import APP_PREFIX, STATUS_WAITING
from config.utils.functions import generateDigitsKey


User.add_to_class('phone', PhoneNumberField(unique=True, null=True))


class ActivationCodeModel(models.Model):
    user = models.ForeignKey(User)
    code = models.CharField(max_length=6, db_index=True)
    dateCreated = models.PositiveIntegerField()
    dateSent = models.PositiveIntegerField(default=0, db_index=True)
    status = models.CharField(max_length=16, default=STATUS_WAITING)
    message = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u"%s - %s" % (self.user.username, self.code)

    class Meta:
        verbose_name = 'activation code '
        verbose_name_plural = 'activation codes'
        db_table = APP_PREFIX + "_acti_code"

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if len(self.code) != 6:
            self.code = generateDigitsKey(length=6)
            self.dateCreated = long(time.time())
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
