from django.contrib import admin

from crawlers.models.portfolioCrawlerModel import PortfolioCrawlerModel

admin.site.register(PortfolioCrawlerModel)
