# -*- coding: utf-8 -*-
import logging

logger = logging.getLogger(__name__)


class BadTypeException(Exception):

    def __init__(self, argument=None):
        self.message = "Your crawler type is invalid, your type is: %s" % ('' if argument is None else argument)
        logger.critical('%s, %s' % (type(self), self.message))
