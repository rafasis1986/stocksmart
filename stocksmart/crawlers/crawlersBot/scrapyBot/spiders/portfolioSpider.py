# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.models import Group, User
from scrapy.selector.unified import Selector
from scrapy.spider import Spider

from crawlers.models.portfolioCrawlerModel import PortfolioCrawlerModel
from stock_app.models.portfolioModel import Portfolio
from stock_app.models.stockModel import Stock
from .exceptions.portfolioExceptions import BadTypeException


logger = logging.getLogger(__name__)

UPDATE_TASK = 'featured-portfolio'
USERNAME = 'StockBeat'
GROUPNAME = 'FEATURED'
SELECT_TYPE_1 = '//a[@class="screener-link-primary"]/text()'
SELECT_TYPE_2 = '//a[@class="tab-link"]/text()'


class PortfolioSpider(Spider):
    name = "portfolios"
    allowed_domains = ["finviz.com"]

    def parse(self, response):
        try:
            crawler = PortfolioCrawlerModel.objects.get(url=response.url)
            if crawler.type == 1:
                symbols = self.getSymbolsType1(response)
            elif crawler.type == 2:
                symbols = self.getSymbolsType2(response)
            else:
                raise BadTypeException(str(crawler.type))
            self.updatePortfolio(crawler.name.upper().strip(), symbols)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))

    def updatePortfolio(self, name, symbols=[]):
        logger.critical('%s, %s' % ('example', 'spider'))
        try:
            user = User.objects.get(username=USERNAME)
            portfolio = Portfolio.objects.get_or_create(name=name, owner=user)[0]
            portfolio.removeAllStocks()
            group = Group.objects.get_or_create(name=GROUPNAME)[0]
            portfolio.groups.add(group)
            stocks = list()
            for item in symbols[1:11]:
                if item.find(' ') == -1:
                    item = str(item).strip().upper()
                    stocks.append(Stock.objects.get_or_create(symbol=item)[0])
            portfolio.addStocks(stocks)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))

    def getSymbolsType1(self, response):
        sel = Selector(response)
        return sel.xpath(SELECT_TYPE_1).extract()

    def getSymbolsType2(self, response):
        sel = Selector(response)
        return sel.xpath(SELECT_TYPE_2).extract()

    def __init__(self):
        crawlers = PortfolioCrawlerModel.objects.filter(task=UPDATE_TASK)
        self.start_urls = []
        for item in crawlers:
            self.start_urls += [item.url]
            item.save()
