# -*- coding: utf-8 -*-
'''
Created on 13/5/2015

@author: rtorres
'''
import datetime


def validate(dateText):
    try:
        datetime.datetime.strptime(dateText, '%d-%m-%y')
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")
