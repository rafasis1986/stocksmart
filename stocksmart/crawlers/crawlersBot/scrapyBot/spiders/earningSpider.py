# -*- coding: utf-8 -*-
import time

from scrapy.spider import Spider

from stock_app.models.stockModel import Stock


LABELS = ['EARNING', 'P/E', 'EPS']


def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


class EarningSpider(Spider):
    name = "earnings"
    allowed_domains = ["finance.yahoo.com"]

    def parse(self, response):
        '''
        filename = response.url.split("/")[-2]
        open(filename, 'wb').write(response.body)
        '''
        title = response.xpath('//title/text()').extract()[0]
        symbol = str(title[:title.find(':')]).upper()
        stock = Stock.objects.get(yahooSymbol=symbol)
        stock.nextEarningTime = long(time.time())
        rows = response.xpath('//table[@id="table1"]//tr')
        rows += response.xpath('//table[@id="table2"]//tr')
        for row in rows:
            if len(row.xpath('th/text()')) > 0:
                for iter in LABELS:
                    if iter in row.xpath('th/text()').extract()[0].upper():
                        try:
                            aux = row.xpath('td/text()').extract()[0]
                            aux = str(aux).strip()
                            if iter == LABELS[0]:
                                stock.nextEarning = aux
                            elif iter == LABELS[1]:
                                stock.pe = float(aux) if isNumber(aux) else 0
                            elif iter == LABELS[2]:
                                stock.eps = float(aux) if isNumber(aux) else 0
                        except Exception as e:
                            print(time.time(), type(e), e.message)
        stock.save()

    def __init__(self):
        prefixUrl = "http://finance.yahoo.com/q?s="
        stocks = Stock.objects.filter(isIndex=False).values('yahooSymbol').order_by('yahooSymbol')
        self.start_urls = []
        for item in stocks:
            self.start_urls += [prefixUrl + str(item['yahooSymbol'])]
