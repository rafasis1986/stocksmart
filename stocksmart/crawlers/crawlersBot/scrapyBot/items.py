# -*- coding: utf-8 -*-
from scrapy.contrib.djangoitem import DjangoItem
from stock_app.models.portfolioModel import Portfolio

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

class PortfolioItem(DjangoItem):
    django_model = Portfolio
