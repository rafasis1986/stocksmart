import time

import schedule
import scrapy.cmdline


def earningJob():
    scrapy.cmdline.execute(argv=['scrapy', 'crawl', 'earnings'])


def portfolioJob():
    scrapy.cmdline.execute(argv=['scrapy', 'crawl', 'portfolios'])


if __name__ == '__main__':
    schedule.every().day.at("15:25").do(portfolioJob)
    schedule.every().day.at("22:00").do(portfolioJob)
    schedule.every().day.at("9:00").do(portfolioJob)
    schedule.every().day.at("23:30").do(earningJob)
    schedule.every().day.at("17:00").do(earningJob)
    schedule.every().day.at("12:00").do(earningJob)
    schedule.every().day.at("9:00").do(earningJob)

    while True:
        schedule.run_pending()
        time.sleep(30)
