# -*- coding: utf-8 -*-
import os
import sys

from django.apps import apps
from django.conf import settings


#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
BOT_NAME = 'earningsBot'

SPIDER_MODULES = ['scrapyBot.spiders']
NEWSPIDER_MODULE = 'scrapyBot.spiders'
LOG_ENABLED = True


# sys.path.append('/home/rtorres/git/stocksmart/stocksmart')
sys.path.append('/webapps/stocksmart-backend/stocksmart')

# os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings.local'
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings.production'


apps.populate(settings.LOCAL_APPS)
# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'earningsBot (+http://www.yourdomain.com)'
