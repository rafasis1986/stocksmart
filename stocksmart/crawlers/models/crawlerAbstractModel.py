# -*- coding: utf-8 -*-
'''
Created on Aug 27, 2015

@author: rtorres
'''
import datetime

from django.db import models


class CrawlerAbstractModel(models.Model):
    url = models.URLField()
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    runs = models.PositiveIntegerField(default=1)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = datetime.datetime.today()
        else:
            self.modified = datetime.datetime.today()
            self.runs += 1
