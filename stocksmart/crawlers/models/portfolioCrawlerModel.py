# -*- coding: utf-8 -*-
'''
Created on Aug 27, 2015

@author: rtorres
'''
from django.db import models
from crawlers.models.crawlerAbstractModel import CrawlerAbstractModel


class PortfolioCrawlerModel(CrawlerAbstractModel):
    name = models.CharField(max_length=64, db_index=True)
    task = models.CharField(max_length=64, db_index=True)
    type = models.PositiveSmallIntegerField(default=1)

    class Meta(CrawlerAbstractModel.Meta):
        abstract = False
        verbose_name_plural = 'Portfolio Crawler'
        verbose_name_plural = 'Portfolios Crawler'
        db_table = 'potfolio_crawler'
        unique_together = ('task', 'url')
        index_together = ['task', 'url']

    def __unicode__(self):
        return u"%s - %s" % (self.name, self.task)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(PortfolioCrawlerModel, self).save()
        self.name = self.name.strip()
        self.task = self.task.strip()
        return models.Model.save(self, force_insert=force_insert, force_update=force_update,
            using=using, update_fields=update_fields)
