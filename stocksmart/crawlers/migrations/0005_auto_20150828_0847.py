# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawlers', '0004_stockcrawlermodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockcrawlermodel',
            name='dateSuccess',
            field=models.DateTimeField(null=True),
        ),
    ]
