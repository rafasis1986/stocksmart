# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawlers', '0002_portfoliocrawlermodel_type'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='portfoliocrawlermodel',
            unique_together=set([('task', 'url')]),
        ),
        migrations.AlterIndexTogether(
            name='portfoliocrawlermodel',
            index_together=set([('task', 'url')]),
        ),
    ]
