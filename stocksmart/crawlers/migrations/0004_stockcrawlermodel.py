# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawlers', '0003_auto_20150828_0834'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockCrawlerModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stockId', models.CharField(max_length=8, db_index=True)),
                ('portfolioId', models.CharField(max_length=8, db_index=True)),
                ('groupId', models.PositiveIntegerField(db_index=True)),
                ('success', models.BooleanField(default=False)),
                ('dateCreated', models.DateTimeField(editable=False)),
                ('dateSuccess', models.DateTimeField()),
            ],
            options={
                'db_table': 'stock_crawler',
                'verbose_name_plural': 'Stocks Crawler',
            },
        ),
    ]
