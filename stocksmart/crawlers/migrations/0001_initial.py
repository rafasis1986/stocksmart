# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PortfolioCrawlerModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('name', models.CharField(max_length=64, db_index=True)),
                ('task', models.CharField(max_length=64, db_index=True)),
                ('runs', models.PositiveIntegerField(default=1)),
            ],
            options={
                'abstract': False,
                'db_table': 'potfolio_crawler',
                'verbose_name_plural': 'Portfolios Crawler',
            },
        ),
        migrations.AlterUniqueTogether(
            name='portfoliocrawlermodel',
            unique_together=set([('task', 'name')]),
        ),
        migrations.AlterIndexTogether(
            name='portfoliocrawlermodel',
            index_together=set([('task', 'name')]),
        ),
    ]
