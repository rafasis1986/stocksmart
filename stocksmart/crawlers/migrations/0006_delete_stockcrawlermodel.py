# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawlers', '0005_auto_20150828_0847'),
    ]

    operations = [
        migrations.DeleteModel(
            name='StockCrawlerModel',
        ),
    ]
