# -*- coding: utf-8 -*-
'''
Created on 9/7/2015

@author: rtorres
'''
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import serializers

from login.serializers import UserSerializer
from login.utils.constants import GROUP_PRIVATE


class UserStockbeSerializer(UserSerializer):
    portfolios = serializers.SerializerMethodField('userPortfolios')

    def userPortfolios(self, user):
        portfolios = user.portfolio_set.all().values('id', 'name', 'dateCreated', 'dateUpdated').order_by('id')
        responseList = [item for item in portfolios]
        groups = user.groups.filter(~Q(name=GROUP_PRIVATE))
        for g in groups:
            portfolios = g.portfolio_set.filter(~Q(owner=user)).values('id', 'name', 'dateCreated', 'dateUpdated').order_by('id')
            responseList += [item for item in portfolios]
        return responseList

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'phone', 'groups', 'portfolios')
        depth = 1


class UserSignUpSerializer(UserStockbeSerializer):

    class Meta(UserStockbeSerializer.Meta):
        fields = ('username', 'email', 'phone')
