# -*- coding: utf-8 -*-
import time

from django.contrib.auth.models import Group
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from phonenumbers.phonenumberutil import NumberParseException
from push_notifications.api.rest_framework import APNSDeviceAuthorizedViewSet

from rest_framework import status
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.status import HTTP_409_CONFLICT
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler
from rest_framework_jwt.settings import api_settings
import phonenumbers

from config.utils.errorResponses import LogicalErrorResponse, TechnichalErrorResponse
from login.models import ActivationCodeModel
from login.utils.errorResponses import PhoneFormatError
from login.utils.exceptions import MissedPhoneError, MissedCodeError, InvalidPhoneError, \
    InvalidCodeError, ExpiredCodeError
from login.utils.functions import jwt_response_payload_handler
from login.utils.functions.phones import formatPhoneToE164
from login.utils.requestTags import PHONE_TAG, CODE_TAG
from login.views import CreateUserView, ActiveUserView, UserDetailView, UsersView
from login_stockbeat.serializers import UserStockbeSerializer
from login_stockbeat.utils.constants import STATUS_EXPIRED, STATUS_ACTIVATED, GROUP_PUBLIC
from stock_app.utils.functions.portfolios import createMainPortfolio


class ActiveUserStockBeView(ActiveUserView):

    @csrf_exempt
    @transaction.atomic
    def post(self, request):
        response = None
        user = None
        try:
            phone = request.data.get(PHONE_TAG)
            code = request.data.get(CODE_TAG)
            if phone is None:
                raise MissedPhoneError
            elif code is None:
                raise MissedCodeError()
            else:
                actiCode = None
                phone = formatPhoneToE164(phone)
                if not phone[1:].isdigit() or len(phone) < 5:
                    raise InvalidPhoneError()
                else:
                    phonenumbers.parse(phone, None)
                try:
                    actiCode = ActivationCodeModel.objects.get(code=code)
                except ActivationCodeModel.DoesNotExist:
                    raise InvalidCodeError()
                if actiCode.user.phone.as_e164 == str(phone):
                    if actiCode.status is STATUS_EXPIRED:
                        raise ExpiredCodeError()
                    else:
                        actiCode.status = STATUS_ACTIVATED
                        user = actiCode.user
                        if user.is_active:
                            raise InvalidCodeError()
                        user.is_active = True
                        user.save()
                        Group.objects.get(name=GROUP_PUBLIC).user_set.add(user)
                        user.save()
                        createMainPortfolio(user)
                        payload = jwt_payload_handler(user)
                        if api_settings.JWT_ALLOW_REFRESH:
                            payload['orig_iat'] = long(time.time())
                        token = jwt_encode_handler(payload)
                        response_data = jwt_response_payload_handler(token, user, request)
                        response = Response(response_data)
                else:
                    raise InvalidPhoneError()
        except NumberParseException as e:
            response = Response(PhoneFormatError(e.message).__dict__, status=HTTP_409_CONFLICT)
        except InvalidCodeError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_401_UNAUTHORIZED)
        except ExpiredCodeError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_401_UNAUTHORIZED)
        except InvalidPhoneError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except ParseError as e:
            response = Response(LogicalErrorResponse(e.detail).__dict__, status=HTTP_409_CONFLICT)
        except Exception as e:
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_401_UNAUTHORIZED)
        return response


class CreateUserStocbeView(CreateUserView):
    serializer_class = UserStockbeSerializer


class UserStockbeDetailView(UserDetailView):

    def get(self, request):
        return Response(UserStockbeSerializer(request.user).data)


class UsersStockbeView(UsersView):
    serializer_class = UserStockbeSerializer


class UsersStockbeViewSet(APNSDeviceAuthorizedViewSet, UserStockbeDetailView):
    pass
