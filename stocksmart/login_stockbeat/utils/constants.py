# -*- coding: utf-8 -*-
'''
Created on 9/7/2015

@author: rtorres
'''
from config.utils.constants import *
from login.utils.constants import *

APP_PREFIX = 'login_stockbe'

GROUP_FEATURED = 'FEATURED'

PREFIX_URL = VERSION_API
