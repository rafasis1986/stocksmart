from django.contrib import admin

from login.models import ActivationCodeModel


admin.site.register(ActivationCodeModel)
