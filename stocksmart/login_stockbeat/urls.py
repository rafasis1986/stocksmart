# -*- coding: utf-8 -*-

from django.conf.urls import url
from rest_framework_jwt.views import RefreshJSONWebToken

from config.utils.constants import PREFIX_URL
from login.views import UserLoginView
from login_stockbeat.views import CreateUserStocbeView, ActiveUserStockBeView, UsersStockbeViewSet


PREFIX_USERS = PREFIX_URL + '/users'

login_patterns = [
    url(PREFIX_USERS + '/login$', UserLoginView.as_view()),
    url(PREFIX_USERS + '/signup$', CreateUserStocbeView.as_view()),
    url(PREFIX_USERS + '/me$', UsersStockbeViewSet.as_view({'put': 'create', 'get': 'get'})),
    url(PREFIX_USERS + '/activate$', ActiveUserStockBeView.as_view()),
    url(PREFIX_USERS + '/token-refresh$', RefreshJSONWebToken.as_view()),
    # url(PREFIX_USERS, UsersStockbeView.as_view()),
]
