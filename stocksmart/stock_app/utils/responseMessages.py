# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''

MSG_ADDED_STOCK_SUCCESS = 'added the value "%s" to your account'

# Format error Message name, phone, type error, error message;
MSG_ERROR_SENDING_CONTACT = "error: %s, %s"
MSG_ERROR_QUERY_TYPE = "you are sending a wrong query type"
MSG_PHONE_ERROR_TIPS = "the phone %s to contact to %s has a invalid format"
MSG_PUSH_TIP = "sent push tip to user: %s"

MSG_SENDING_TIPS_SMS = "We will send a text message with the stock informations to all users that you added"
MSG_SYMBOL_STOCK_MISSED = 'missed symbol stock'

TEMPLATE_MESSAGE_TEXT_TIPS = "Your friend %s thinks %s @ %s is a %s - get www.stockbe.at for free to track & share stock tips with friends"
