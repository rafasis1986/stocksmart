# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''

MSG_CLEAN_PORTFOLIO_ERROR = 'One error has been occurred while cleaned a portfolio'
MSG_STOCK_DONT_FIND_ERROR = 'the system dont find the stock in the web'
MSG_STOCK_DONT_EXIST_ERROR = 'the system dont find the stock into the data base'
MSG_STOCK_DONT_SERIALIZED_ERROR = "The stock '%s' has problem to be serialized"
MSG_TIP_DONT_EXIST_ERROR = 'your tip id do not exist in the system'
MSG_USER_HAS_STOCK_ERROR = 'you add this stock to your account previously'
MSG_USER_PORTFOLIO_EXIST_ERROR = 'The user have one list of the stocks with same name.'
MSG_USER_TIP_EXIST_ERROR = 'The user has added previously your sentiment about the current tip.'
