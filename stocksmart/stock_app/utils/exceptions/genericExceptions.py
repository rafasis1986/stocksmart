# -*- coding: utf-8 -*-
'''
Created on Aug 27, 2015

@author: rtorres
'''
import logging
from django.core.exceptions import ValidationError

logger = logging.getLogger(__name__)


class TechnicalException(Exception):

    def __init__(self, argument=None):
        self.message = "A technical error has occurred, info: %s" % ('' if argument is None else argument)
        logger.critical('%s, %s' % (type(self), self.message))


class LogicalException(Exception):

    def __init__(self, argument=None):
        self.message = "check your request, a logical error has occurred, info: %s" % ('' if argument is None else argument)
        logger.critical('%s, %s' % (type(self), self.message))


class ValidationException(ValidationError):
    def __init__(self, argument=None):
        self.message = "check your argument" if argument is None else argument
        logger.critical('%s, %s, %s' % (type(self), self.message, '' if argument is None else argument))
