# -*- coding: utf-8 -*-
'''
Created on 14/5/2015

@author: rtorres
'''


class MissedArgumentException(Exception):

    def __init__(self, argument=None):
        self.message = "You forgotten send %s" % ('some arguments' if argument is None else argument)


class NoStockGoogleException(Exception):

    def __init__(self, argument=None):
        self.message = "No was found your stock symbol '%s' in google" % ('' if argument is None else argument)


class UserDontExistException(Exception):

    def __init__(self, argument=None):
        self.message = "No was found one user account with username '%s'" % ('' if argument is None else argument)


class StockDontExistException(Exception):

    def __init__(self, argument=None):
        self.message = "No was found your stock symbol '%s'" % ('' if argument is None else argument)


class TipsStatusException(Exception):

    def __init__(self, argument=None):
        self.message = "The tips has been previously sent to users"


class MissedContactNameException(Exception):

    def __init__(self):
        self.message = "verify your contacts names before to send the request"


class MissedContactPhoneException(Exception):

    def __init__(self):
        self.message = "verify your contacts phones before to send the request"


class AttemptSentTipsExpiredException(Exception):

    def __init__(self):
        self.message = "The stocks tips rebase the number of attempts permitted"


class WrongTypeTipsException(Exception):

    def __init__(self):
        self.message = "The type of of the tips is not valid value"
