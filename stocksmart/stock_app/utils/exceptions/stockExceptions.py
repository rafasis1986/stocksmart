'''
Created on Aug 20, 2015

@author: rtorres
'''
import logging

from stock_app.utils.exceptions.genericExceptions import LogicalException
from django.core.exceptions import ValidationError
from stock_app.models.yahooSymbolModel import YahooSymbolModel


logger = logging.getLogger(__name__)


class StockDontExistException(LogicalException):

    def __init__(self, argument=None):
        self.message = "No was found your stock '%s'" % ('' if argument is None else argument)
        logger.critical('%s, %s' % (type(self), self.message))


class DontExistGoogleSymbolException(ValidationError):

    def __init__(self, argument=None):
        self.message = "No was found in google finance your symbol '%s'" % ('' if argument is None else argument)
        logger.critical('%s, %s' % (type(self), self.message))


class DontFindYahooSymbolException(ValidationError):

    def __init__(self, argument):
        YahooSymbolModel.objects.create(currentSymbol=argument)
        self.message = "No was found in yahoo finance your symbol '%s'" % ('' if argument is None else argument)
        logger.critical('%s, %s' % (type(self), self.message))
