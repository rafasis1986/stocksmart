'''
Created on Aug 20, 2015

@author: rtorres
'''

import logging

logger = logging.getLogger(__name__)


class HistoricalToStockException(Exception):
    ''''
    class to generate exception ocurrence in some of next functions:
    - addHistoricalToStock
    - addHistoricalToStockByYears
    '''

    def __init__(self, argument=None):
        self.message = "Error append historical data to stock" if argument is None else argument
        logger.critical('%s, %s' % (type(self), self.message))
