# -*- coding: utf-8 -*-
'''
Created on Aug 25, 2015

@author: rtorres
'''
import logging

logger = logging.getLogger(__name__)


class UserDontExistContactException(Exception):

    def __init__(self, argument=None):
        self.message = "The user phone does not exist in the tip contact list."
        logger.critical('%s, %s' % (type(self), self.message))


class UserPreviouslySentimentException(Exception):

    def __init__(self, argument=None):
        self.message = "The user has added previously the sentiment about this tip."
        logger.critical('%s, %s' % (type(self), self.message))


class UserPreviouslySenTipException(Exception):

    def __init__(self, argument=None):
        symbol = '' if argument is None else argument
        self.message = "You already shared '%s' today! You can only recommend each symbol once a day per day." % symbol
        logger.critical('%s, %s' % (type(self), self.message))
