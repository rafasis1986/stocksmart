# -*- coding: utf-8 -*-
'''
Created on Sep 11, 2015

@author: rtorres
'''
import logging

logger = logging.getLogger(__name__)


class StockNewsException(Exception):

    def __init__(self, argument=None):
        symbol = '' if argument is None else argument
        self.message = "Problem getting news to symbol '%s' " % symbol
        logger.critical('%s, %s' % (type(self), self.message))
