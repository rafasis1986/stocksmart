# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''
import logging

from config.utils.errorResponses import LogicalErrorResponse, TechnichalErrorResponse
from stock_app.utils.errorMessages import MSG_USER_HAS_STOCK_ERROR, MSG_STOCK_DONT_EXIST_ERROR


logger = logging.getLogger(__name__)


class UserHasStockError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(UserHasStockError, self).__init__(MSG_USER_HAS_STOCK_ERROR if message is None else message)
        self.code = 2008
        self.error = self.__class__.__name__
        logger.error('%s, %s, %s' % (self.error, self.code, self.message))


class StockDontExistError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(StockDontExistError, self).__init__(MSG_STOCK_DONT_EXIST_ERROR if message is None else message)
        self.code = 2009
        self.error = self.__class__.__name__
        logger.error('%s, %s, %s' % (self.error, self.code, self.message))


class StockDontFindError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(StockDontFindError, self).__init__(message)
        self.code = 2010
        self.error = self.__class__.__name__
        logger.error('%s, %s, %s' % (self.error, self.code, self.message))


class PortfolioDuplicatedError(TechnichalErrorResponse):

    def __init__(self, message=None):
        super(PortfolioDuplicatedError, self).__init__(message)
        self.code = 1002
        self.error = self.__class__.__name__
        logger.error('%s, %s, %s' % (self.error, self.code, self.message))


class MissedArgumentError(LogicalErrorResponse):

    def __init__(self, message=None):
        super(MissedArgumentError, self).__init__(message)
        self.code = 2011
        self.error = self.__class__.__name__
        logger.error('%s, %s, %s' % (self.error, self.code, self.message))
