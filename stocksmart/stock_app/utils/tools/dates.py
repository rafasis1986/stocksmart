'''
Created on Aug 20, 2015

@author: rtorres
'''

def getDatesToQuarterYear(numQuarter=1):
    '''
    return list with start and end date in format %m-%d
    '''
    response = ['01-01', '03-31']
    if numQuarter == 2:
        response = ['04-01', '06-30']
    elif numQuarter == 3:
        response = ['07-01', '09-30']
    elif numQuarter == 4:
        response = ['10-01', '12-31']
    return response
