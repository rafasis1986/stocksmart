'''
Created on Aug 20, 2015

@author: rtorres
'''
from datetime import datetime


def timeIsInWorkingTime(timeEval=None):
    response = False
    if type(timeEval) is datetime:
        if timeEval.weekday() > 5 or timeEval.hour > 16:
            response = False
        elif timeEval.hour == 16 and timeEval.minute > 30:
            response = False
        elif timeEval.hour == 9 and timeEval.minute < 30:
            response = False
        else:
            response = True
    return response
