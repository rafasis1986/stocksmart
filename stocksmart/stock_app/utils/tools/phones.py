'''
Created on Aug 20, 2015

@author: rtorres
'''
from login.utils.exceptions import MissedPhoneError, InvalidPhoneError


def verifyPhone(phone):
    if phone is None:
        raise MissedPhoneError()
    auxPhone = phone.replace('(', '').replace(')', '').replace('-', '').replace('+', ''). \
        replace(' ', '').replace('#', '').replace('_', '').replace('.', '')
    if not auxPhone.isdigit() and len(auxPhone) < 5:
        raise InvalidPhoneError()
    return '+' + str(auxPhone)
