'''
Created on Aug 20, 2015

@author: rtorres
'''
import logging

from django.contrib.auth.models import User, Group
from django.db import transaction

from stock_app.models.portfolioModel import Portfolio
from stock_app.utils.constants import DEFAULT_PORTFOLIO_NAME, GROUP_PRIVATE


logger = logging.getLogger(__name__)


@transaction.atomic
def createMainPortfolio(user=None):
    response = False
    if type(user) is User:
        portfolio = Portfolio()
        portfolio.owner = user
        portfolio.name = DEFAULT_PORTFOLIO_NAME
        portfolio.save()
        group = Group.objects.get(name=GROUP_PRIVATE)
        portfolio.groups.add(group)
        portfolio.save()
        response = True
    return response

