'''
Created on Aug 20, 2015

@author: rtorres
'''
from datetime import date, datetime
import logging

from django.db import transaction
from django.db.utils import IntegrityError
from yahoo_finance import Share

from stock_app.models.indexModel import IndexModel
from stock_app.models.stockHistoryModel import StockHistory
from stock_app.models.stockModel import Stock
from stock_app.utils.classes.threadWithReturnValue import ThreadWithReturnValue
from stock_app.utils.constants import DATE_INIT_YEAR_STR, DATE_END_YEAR_STR, YEAR_RANGE_RECENT
from stock_app.utils.exceptions import StockDontExistException
from stock_app.utils.exceptions.historicalExceptions import HistoricalToStockException
from stock_app.utils.functions.castings import fromDicToStockHistoryModel
from stock_app.utils.requestTags import ERROR_TAG
from stock_app.utils.responseTags import RESULT, STOCK_ID


logger = logging.getLogger(__name__)


@transaction.atomic
def setAllPreviousDayClose():
    histories = StockHistory.objects.order_by('stock', '-date').distinct('stock')
    for item in histories:
        item.stock.closePrice = item.close
        item.stock.save()


@transaction.atomic
def addHistoricalToStock(stockId, dateFrom, dateTo):
    historyList = list()
    response = {RESULT: False, STOCK_ID: stockId}
    try:
        stock = Stock.objects.get(id=stockId)
        name = stock.yahooSymbol
        if stock.isIndex:
            index = IndexModel.objects.get(stock__id=stock.id)
            name = index.yahooSymbol
        init = dateFrom.strftime('%Y-%m-%d')
        end = dateTo.strftime('%Y-%m-%d')
        yahooStock = Share(name)
        historical = yahooStock.get_historical(init, end)
        threadList = list()
        for item in historical:
            t = ThreadWithReturnValue(target=fromDicToStockHistoryModel, args=(stock, item))
            threadList.append(t)
        for t in threadList:
            t.start()
        for t in threadList:
            historyList.append(t.join())
        if len(historyList) > 0:
            StockHistory.objects.bulk_create(historyList)
            response.update({RESULT: True})
    except Stock.DoesNotExist as e:
        e = StockDontExistException(stockId)
        response.update({ERROR_TAG: e.message})
    except ValueError as e:
        response.update({ERROR_TAG: e.message})
    except IntegrityError as e:
        response.update({ERROR_TAG: e.message})
    except Exception as e:
        response.update({ERROR_TAG: e.message})
    return response


def addHistoricalToStockByYears(stockId, yearFrom, yearTo):
    '''
    @function to get historical data from yahoo api, between a year range
    @param stockID: string with id stock
    @param yearFrom: positive integer
    @param yearTo: positive integer (must be greater that @yearFrom)
    '''
    logger.info("Start addHistoricalToStockByYears(%s, %s, %s)" % (stockId, yearFrom, yearTo))
    yearRange = yearTo - yearFrom + 1
    today = date.today()
    dateNow = str(today.year) + '-' + '%02d' % today.month + '-' + '%02d' % today.day
    try:
        threads = list()
        responses = list()
        for i in range(0, yearRange):
            year = str(yearTo - i)
            logger.info("Start StockId: %s, Year: %s " % (stockId, year))
            if today.year >= (yearTo - i):
                initStrDate = year + '-' + DATE_INIT_YEAR_STR
                if initStrDate < dateNow:
                    endStrDate = year + '-' + DATE_END_YEAR_STR
                    if endStrDate > dateNow:
                        endStrDate = dateNow
                    initDate = datetime.strptime(initStrDate, '%Y-%m-%d')
                    endDate = datetime.strptime(endStrDate, '%Y-%m-%d')
                    t = ThreadWithReturnValue(target=addHistoricalToStock, args=(stockId, initDate, endDate))
                    threads.append(t)
        for t in threads:
            t.start()
        for t in threads:
            responses.append(t.join())
        for item in responses:
            if ((item.get(RESULT) is not None) and (item.get(RESULT) is False)):
                logger.info("Success StockId: %s, Year: %s" % (item.get(STOCK_ID), year))
        logger.info("Success addHistoricalToStockByYears()")
    except Exception as e:
        logger.critical("Exception addHistoricalToStockByYears() exception: %s, message: %s" % (type(e), e.message))
        raise HistoricalToStockException()


def addHistoricalStockRecent(stockId):
    '''
    @function to get historical data from yahoo api, to currentYear
    @param stockID: string with id stock
    '''
    logger.info("Start addHistoricalStockRecent(%s)" % (stockId))
    today = date.today()
    initStrDate = str(today.year) + '-' + DATE_INIT_YEAR_STR
    dateNow = str(today.year) + '-' + '%02d' % today.month + '-' + '%02d' % today.day
    initDate = datetime.strptime(initStrDate, '%Y-%m-%d')
    endDate = datetime.strptime(dateNow, '%Y-%m-%d')
    try:
        stock = Stock.objects.get(id=stockId)
        if stock.hasRecentHistory is False:
            response = addHistoricalToStock(stockId, initDate, endDate)
            if ((response.get(RESULT) is not None) and (response.get(RESULT) is False)):
                    logger.info("Error StockId: %s, Year: %s" % (response.get(STOCK_ID), today.year))
            logger.info("Success addHistoricalStockRecent()")
            stock.hasRecentHistory = True
            stock.save()
        if stock.hasHistory is False:
            addHistoricalToStockByYears(stockId, today.year - YEAR_RANGE_RECENT, today.year - 1)
            stock.hasHistory = True
            stock.save()
    except Exception as e:
        logger.critical("Exception addHistoricalStockRecent() exception: %s, message: %s" % (type(e), e.message))
