'''
Created on Aug 20, 2015

@author: rtorres
'''
import time

from googlefinance import getQuotes

from config.utils.errorResponses import TechnichalErrorResponse
from stock_app.utils.errorMessages import MSG_STOCK_DONT_FIND_ERROR
from stock_app.utils.errorResponses import StockDontFindError
from stock_app.utils.requestTags import ERROR_TAG


def findStockGoogle(symbol):
    '''
    @param symbol: type str
    @return: dictionary with Stock model values without "error" tag,
            in case to raise error this dictionary contain tag "error"
            with error message
    '''
    answer = dict()
    try:
        aux = getQuotes(symbol)
        last_price = str(round(float(aux[0].get('LastTradePrice').replace(',', '')), 2))
        answer.update({'index': aux[0].get('Index')})
        answer.update({'name': aux[0].get('StockSymbol')})
        answer.update({'symbol': aux[0].get('StockSymbol')})
        answer.update({'yahooSymbol': aux[0].get('StockSymbol')})
        answer.update({'currentPrice': last_price})
        answer.update({'date_last_consulted': long(time.time())})
    except IOError as e:
        answer.update({ERROR_TAG: StockDontFindError(MSG_STOCK_DONT_FIND_ERROR)})
    except TypeError:
        answer.update({ERROR_TAG: TechnichalErrorResponse(e.message)})
    except:
        answer.update({ERROR_TAG: TechnichalErrorResponse()})
    return answer


def getCurrentTradeValues(stocks=[]):
    stocks_names = []
    trades_list = []
    for item in stocks:
        stocks_names.append(str(item.compositeName))
    if len(stocks_names) > 0:
        trades_list = getQuotes(stocks_names)
    return trades_list
