# -*- coding: utf-8 -*-
'''
Created on Aug 20, 2015

@author: rtorres
'''
import logging
import socket
import time

from django.db import transaction

from stock_app.models.indexModel import IndexModel
from stock_app.models.stockModel import Stock
from stock_app.utils.constants import MAX_STOCKS_API
from stock_app.utils.tools import stockretriever
from stock_app.utils.tools.stockretriever import QueryError


logger = logging.getLogger(__name__)


@transaction.atomic
def updateStatistics(responseStock):
    currentTime = long(time.time())
    for item in responseStock:
        try:
            name = '...' if item['Name'] is None else str(item['Name'])
            fiftyDayAvg = 0 if item['FiftydayMovingAverage'] is None else float(item['FiftydayMovingAverage'])
            twoHundredDayAvg = 0 if item['TwoHundreddayMovingAverage'] is None else float(item['TwoHundreddayMovingAverage'])
            previousDayClose = 0 if item['PreviousClose'] is None else float(item['PreviousClose'])
            yearHigh = 0 if item['YearHigh'] is None else float(item['YearHigh'])
            yearLow = 0 if item['YearLow'] is None else float(item['YearLow'])
            symbol = item['Symbol']
            if symbol[0] == '^':
                symbol = IndexModel.objects.get(yahooSymbol=symbol).stock.symbol
            Stock.objects.filter(yahooSymbol=symbol).update(name=name,
                fiftyDayAvg=fiftyDayAvg, twoHundredDayAvg=twoHundredDayAvg,
                previousDayClose=previousDayClose, yearHigh=yearHigh, yearLow=yearLow,
                dateLastConsulted=currentTime)
        except Exception as e:
            logger.critical('symbol: %s, %s, %s' % (item['Symbol'], type(e), e.message))


def updateAllSotckStatistics():
    try:
        symbols = Stock.objects.filter(isIndex=False).values_list('yahooSymbol').order_by('yahooSymbol')
        symbolsList = [str(item[0]) for item in symbols]
        chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
        for stocksList in chunks(symbolsList, MAX_STOCKS_API):
            try:
                responseStock = stockretriever.get_current_info(stocksList,
                    columnsToRetrieve=['Symbol', 'Name', 'Open', 'FiftydayMovingAverage',
                    'TwoHundreddayMovingAverage', 'PreviousClose', 'YearHigh', 'YearLow'])
                updateStatistics(responseStock)
            except QueryError as e:
                for item in stocksList:
                    responseStock = stockretriever.get_current_info([item],
                        columnsToRetrieve=['Symbol', 'Name', 'Open', 'FiftydayMovingAverage',
                        'TwoHundreddayMovingAverage', 'PreviousClose', 'YearHigh', 'YearLow'])
                    updateStatistics([responseStock])
            except Exception as e:
                logger.critical('stockList: %s, %s, %s' % (stocksList, type(e), e.message))
    except socket.error as e:
        logger.critical('%s, %s' % (type(e), e.message))
    except Exception as e:
        logger.critical('%s, %s' % (type(e), e.message))


@transaction.atomic
def updateClosePrice(responseStock):
    for item in responseStock:
        try:
            closePrice = 0 if item['LastTradePriceOnly'] is None else float(item['LastTradePriceOnly'])
            symbol = item['Symbol']
            if symbol[0] == '^':
                symbol = IndexModel.objects.get(yahooSymbol=symbol).stock.symbol
            if closePrice != 0:
                s = Stock.objects.get(yahooSymbol=symbol)
                s.closePrice = closePrice
                s.save()
        except Exception as e:
            logger.critical('symbol: %s, %s, %s' % (item['Symbol'], type(e), e.message))


def updateAllClosePrice():
    '''
    run this functions before 16:15 New York Time
    '''
    try:
        symbols = Stock.objects.filter(isIndex=False).values_list('yahooSymbol').order_by('yahooSymbol')
        symbolsList = [str(item[0]) for item in symbols]
        chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
        for stocksList in chunks(symbolsList, MAX_STOCKS_API):
            try:
                responseStock = stockretriever.get_current_info(stocksList,
                    columnsToRetrieve=['Symbol', 'LastTradePriceOnly'])
                updateClosePrice(responseStock)
            except QueryError as e:
                for item in stocksList:
                    try:
                        responseStock = stockretriever.get_current_info([item],
                            columnsToRetrieve=['Symbol', 'LastTradePriceOnly'])
                        updateClosePrice([responseStock])
                    except Exception as e:
                        logger.critical('symbol: %s, %s' % (item, type(e)))
            except Exception as e:
                logger.critical('stockList: %s, %s, %s' % (stocksList, type(e), e.message))
    except socket.error as e:
        logger.critical('%s, %s' % (type(e), e.message))
    except Exception as e:
        logger.critical('%s, %s' % (type(e), e.message))


@transaction.atomic
def updateOpenPrice(responseStock):
    for item in responseStock:
        try:
            symbol = item.get('Symbol')
            if symbol[0] == '^':
                symbol = IndexModel.objects.get(yahooSymbol=symbol).stock.symbol
            openPrice = 0 if item.get('Open') is None else float(item['Open'])
            previousDayClose = 0 if item.get('PreviousClose') is None else float(item.get('PreviousClose'))
            if openPrice == 0:
                openPrice = previousDayClose
            if openPrice != 0:
                s = Stock.objects.get(yahooSymbol=symbol)
                s.openPrice = openPrice
                s.previousDayClose = previousDayClose
                s.save()
        except Exception as e:
            logger.critical('symbol: %s, %s, %s' % (item['Symbol'], type(e), e.message))


def updateAllOpenPrice():
    '''
    run this functions before 09:01 to New York Time
    '''
    try:
        symbols = Stock.objects.filter(isIndex=False).values_list('yahooSymbol').order_by('yahooSymbol')
        symbolsList = [str(item[0]) for item in symbols]
        chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
        for stocksList in chunks(symbolsList, MAX_STOCKS_API):
            try:
                responseStock = stockretriever.get_current_info(stocksList, columnsToRetrieve=['Symbol',
                    'Open', 'PreviousClose', 'LastTradePriceOnly'])
                updateOpenPrice(responseStock)
            except QueryError as e:
                for item in stocksList:
                    try:
                        responseStock = stockretriever.get_current_info([item],
                            columnsToRetrieve=['Symbol', 'Open', 'PreviousClose', 'LastTradePriceOnly'])
                        updateOpenPrice([responseStock])
                    except Exception as e:
                        logger.critical('symbol: %s, %s' % (item, type(e)))
            except Exception as e:
                logger.critical('stockList: %s, %s, %s' % (stocksList, type(e), e.message))
    except socket.error as e:
        logger.critical('%s, %s' % (type(e), e.message))
    except Exception as e:
        logger.critical('%s, %s' % (type(e), e.message))
