'''
Created on Aug 20, 2015

@author: rtorres
'''
from django.core.cache import cache
from stock_app.utils.constants import VAR_CURRENT_PRICE_CACHE, TIME_CURRENT_PRICE_CACHE


def findCurrentPriceCache(symbol=''):
    return cache.get(VAR_CURRENT_PRICE_CACHE, {symbol: Exception()}).get(symbol.strip().upper())


def saveCurrentPriceCache(symbol='', currentPrice=0):
    prices = cache.get(VAR_CURRENT_PRICE_CACHE)
    if prices is None:
        prices = dict()
    prices.update({str(symbol): str(currentPrice)})
    cache.set(VAR_CURRENT_PRICE_CACHE, prices, TIME_CURRENT_PRICE_CACHE)
