'''
Created on Aug 20, 2015

@author: rtorres
'''
from threading import Thread

from config.utils.customExceptions import SerializerException
from config.utils.errorResponses import TechnichalErrorResponse
from stock_app.models.stockModel import Stock
from stock_app.serializers.stockSerializers import StockSerializer
from stock_app.utils.errorMessages import MSG_STOCK_DONT_SERIALIZED_ERROR
from stock_app.utils.errorResponses import StockDontExistError
from stock_app.utils.exceptions import NoStockGoogleException
from stock_app.utils.exceptions.stockExceptions import StockDontExistException
from stock_app.utils.functions.cache import saveCurrentPriceCache
from stock_app.utils.functions.google import findStockGoogle
from stock_app.utils.functions.historicals import addHistoricalStockRecent
from stock_app.utils.requestTags import STOCK_TAG, ERROR_TAG


def findStockModel(name):
    '''
    @param name: type str
    @return: dictionary with one Stock model without "error" tag,
            in case to raise error this dictionary contain tag "error"
            with error message or error object
    '''
    answer = dict()
    try:
        index = name.find(':')
        stock_name = ''
        stock_index = ''
        if index > 0:
            stock_index = name[:index]
            stock_name = name[index + 1:]
            stock = Stock.objects.get(symbol=stock_name, index=stock_index)
        else:
            stock_name = name
            stock = Stock.objects.get(symbol=stock_name)
        answer.update({STOCK_TAG: stock})
    except Stock.DoesNotExist:
        answer.update({ERROR_TAG: StockDontExistError()})
    except:
        answer.update({ERROR_TAG: TechnichalErrorResponse()})
    return answer


def getOrSaveStockListFromSymbolNames(stocksNames):
    stocks = list()
    for item in stocksNames:
        answer = findStockModel(item.strip().upper())
        if answer.get(STOCK_TAG).__class__ is Stock:
            stocks.append(answer.get(STOCK_TAG))
        elif answer.get(ERROR_TAG) and answer.get(ERROR_TAG).__class__ is StockDontExistError:
            stock = findStockGoogle(item.strip().upper())
            if not stock.get(ERROR_TAG):
                saveCurrentPriceCache(symbol=stock.get('symbol'), currentPrice=stock.get('currentPrice'))
                serializer = StockSerializer(data=stock)
                if serializer.is_valid():
                    stock = serializer.save()
                    stocks.append(stock)
                else:
                    raise SerializerException(MSG_STOCK_DONT_SERIALIZED_ERROR % item)
            else:
                raise NoStockGoogleException(argument=item)
        else:
            raise Exception("Response Error")
    return stocks


def findStockListFromSymbolNames(stocksNames):
    stocks = list()
    for item in stocksNames:
        answer = findStockModel(item.strip().upper())
        if answer.get(STOCK_TAG).__class__ is Stock:
            stocks.append(answer.get(STOCK_TAG))
        elif answer.get(ERROR_TAG) and answer.get(ERROR_TAG).__class__ is StockDontExistError:
            raise StockDontExistException(argument=item)
    return stocks


def initStocksInfo(stocks):
    for s in stocks:
        t1 = Thread(target=addHistoricalStockRecent, args=(s.id,))
        t1.start()
