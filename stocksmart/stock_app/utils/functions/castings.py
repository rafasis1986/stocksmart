'''
Created on Aug 20, 2015

@author: rtorres
'''

from datetime import datetime

from stock_app.models.stockHistoryModel import StockHistory


def fromDicToStockHistoryModel(stock, item):
    historical = StockHistory()
    historical.stock = stock
    if stock.isIndex:
        historical.volume = long(item['Volume'].replace(',', '')) / 1000
    else:
        historical.volume = long(item['Volume'].replace(',', ''))
    historical.close = float(item['Close'].replace(',', '.'))
    historical.high = float(item['High'].replace(',', '.'))
    historical.low = float(item['Low'].replace(',', '.'))
    historical.open = float(item['Open'].replace(',', '.'))
    auxDate = datetime.strptime(item['Date'], '%Y-%m-%d')
    historical.date = long(auxDate.strftime('%s'))
    return historical
