'''
Created on Aug 20, 2015

@author: rtorres
'''
import logging
import time

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from django.db import transaction
from push_notifications.models import APNSDevice
from twilio.rest.client import TwilioRestClient
from twilio.rest.exceptions import TwilioRestException

from stock_app.models.intentTipsModel import IntentTipsModel
from stock_app.models.stockTipsModel import StockTipsModel
from stock_app.utils.constants import STATUS_WAITING, STATUS_SENT, STATUS_ERROR
from stock_app.utils.exceptions import AttemptSentTipsExpiredException, TipsStatusException
from stock_app.utils.responseMessages import TEMPLATE_MESSAGE_TEXT_TIPS, MSG_ERROR_SENDING_CONTACT, \
    MSG_PUSH_TIP


logger = logging.getLogger(__name__)


@transaction.atomic
def sendStockTip(tipsId):
    tipObject = None
    intent = None
    try:
        tipObject = StockTipsModel.objects.get(id=tipsId)
        try:
            intent = IntentTipsModel.objects.get(id=tipObject.id)
            if intent.intent > 2:
                raise AttemptSentTipsExpiredException()
        except Exception:
            pass
        if tipObject.status != STATUS_WAITING:
            raise TipsStatusException()
        friendName = tipObject.owner.email.split('@')[0]
        messageText = TEMPLATE_MESSAGE_TEXT_TIPS % (friendName, tipObject.stock.symbol,
            tipObject.tipPrice, tipObject.type.upper())
        client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
        tipObject.dateSent = long(time.time())
        for contact in tipObject.contactmodel_set.filter(status=''):
            status = ''
            try:
                user = User.objects.get(phone__contains=contact.phone)
                device = APNSDevice.objects.get(user=user, active=True)
                device.send_message(messageText)
                status = MSG_PUSH_TIP % user.username
            except (MultipleObjectsReturned, User.DoesNotExist, APNSDevice.DoesNotExist) as e:
                try:
                    message = client.messages.create(
                        to=str(contact.phone),
                        from_=settings.TWILIO_PHONE_NUMBER,
                        body=messageText,
                    )
                    status = message.sid
                except TwilioRestException as e:
                    logger.critical('%s, %s, contactId: %s' % (type(e), e.message, contact.id))
                    status = MSG_ERROR_SENDING_CONTACT % (type(e), e.msg)
                except Exception as e:
                    logger.critical('%s, %s, contactId: %s' % (type(e), e.message, contact.id))
                    status = MSG_ERROR_SENDING_CONTACT % (type(e), e.message)
            except Exception as e:
                logger.critical('%s, %s, contactId: %s' % (type(e), e.message, contact.id))
                status = type(e)
            finally:
                contact.status = status
                contact.save()
        tipObject.status = STATUS_SENT
        tipObject.message = messageText
        tipObject.save()
        if settings.TWILIO_ENVIROMENT_TEST and settings.TWILIO_PHONE_TESTING:
            message = client.messages.create(to=settings.TWILIO_PHONE_TESTING,
                                             from_=settings.TWILIO_PHONE_NUMBER,
                                             body=messageText,
                                             )
            status = message.sid
    except StockTipsModel.DoesNotExist:
        tipObject = None
    except AttemptSentTipsExpiredException as e:
        tipObject.message += e.message
    except TipsStatusException as e:
        tipObject.message = e.message
        tipObject.save()
    except Exception as e:
        if type(tipObject) is StockTipsModel:
            tipObject.status = STATUS_ERROR
            tipObject.message = e.message
            tipObject.save()
    finally:
        try:
            intent = IntentTipsModel.objects.get(id=tipsId)
        except IntentTipsModel.DoesNotExist:
            intent = IntentTipsModel()
            intent.id = tipsId
        finally:
            intent.save()
