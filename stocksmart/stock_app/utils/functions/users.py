'''
Created on Aug 20, 2015

@author: rtorres
'''
from django.contrib.auth.models import User

from stock_app.utils.exceptions import UserDontExistException


def findUserListFromUsernames(userNames):
    users = list()
    for item in userNames:
        try:
            user = User.objects.get(username=item)
            users.append(user)
        except User.DoesNotExist:
            raise UserDontExistException(argument=item)
    return users