# -*- coding: utf-8 -*-
'''
Created on Sep 11, 2015

@author: rtorres
'''
import datetime
import logging
from threading import Thread
import time

from django.core.cache import cache

from stock_app.models.stockModel import Stock
from stock_app.models.stockNewsModel import StockNewsModel
from stock_app.utils.constants import LOCK_EXPIRE_LONG, VAR_TEMPLATE_NEWS, VAR_GET_NEWS, \
    VAR_YAHOO_NAMES_CACHE, VAR_UPDATE_NEWS
from stock_app.utils.exceptions.stockExceptions import DontFindYahooSymbolException
from stock_app.utils.tools.stockretriever import get_news_feed


REGEX_PUB_DATE_NEWS = "%a, %d %b %Y %H:%M:%S %Z"
logger = logging.getLogger(__name__)


def updateNews():
    logger.info("Start updateNews()")
    try:
        symbols = cache.get(VAR_YAHOO_NAMES_CACHE, list())
        for item in symbols:
            stock = Stock.objects.get(yahooSymbol=item)
            news = cache.get(VAR_TEMPLATE_NEWS % item, list())
            newsList = list()
            for feed in news:
                sNews = StockNewsModel()
                sNews.desc = feed.get('description')
                sNews.title = feed.get('title')
                sNews.link = feed.get('link').split('*')[-1]
                pubDate = feed.get('pubDate')
                dt = datetime.datetime.strptime(pubDate, REGEX_PUB_DATE_NEWS).timetuple()
                sNews.pubDate = int(time.mktime(dt))
                sNews.yahooSymbol = item
                sNews.stock = stock
                newsList.append(sNews)
            if len(news) > 0:
                StockNewsModel.objects.filter(stock=stock).delete()
                StockNewsModel.objects.bulk_create(newsList)
    except Exception as e:
        logger.critical('Error: %s, %s' % (type(e), e.message))
    finally:
        cache.delete(VAR_UPDATE_NEWS)
    logger.info("End updateNews()")


def getStockNews(symbol):
    try:
        symbol = str(symbol)
        response = get_news_feed(symbol)
        cache.add(VAR_TEMPLATE_NEWS % symbol, response, LOCK_EXPIRE_LONG * 10)
    except KeyError as e:
        DontFindYahooSymbolException(argument=symbol)
    except Exception as e:
        logger.critical('Symbol: %s, %s, %s' % (symbol, type(e), e.message))


def addNews():
    logger.info("Start addNews()")
    try:
        stockNames = cache.get(VAR_YAHOO_NAMES_CACHE, list())
        chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
        for stocksList in chunks(stockNames, 10):
            threads = list()
            for item in stocksList:
                tAux = Thread(target=getStockNews, args=(item,))
                threads.append(tAux)
            for t in threads:
                try:
                    t.start()
                except Exception as e:
                    logger.error("Task Exception: %e message = %s  " % (type(e), e.message))
            for t in threads:
                t.join()
    except Exception as e:
        logger.error("Task Exception: %s message = %s  " % (type(e), e.message))
    finally:
        cache.delete(VAR_GET_NEWS)
    logger.info("End addNews()")
