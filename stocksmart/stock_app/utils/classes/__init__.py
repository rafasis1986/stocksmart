# -*- coding: utf-8 -*-
'''
Created on 21/4/2015

@author: rtorres
'''

#from tweepy.streaming import StreamListener

from ast import *
from threading import Thread


#===================================================================================================
# from stock_app.models import Stock, StockStream
#===================================================================================================
class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs, Verbose)
        self._return = None

    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args,
                                                **self._Thread__kwargs)

    def join(self):
        Thread.join(self)
        return self._return


#===================================================================================================
# class StockStreamtListener(StreamListener):
#
#     """
#     A listener handles tweets are the received from the stream.
#     This listener that save specific fields from received tweets to system database.
#     """
#     stock_id = 0
#
#     def __init__(self, stock_id=None):
#         super(StockStreamtListener, self).__init__()
#         self.stock_id = stock_id
#
#     def on_data(self, data):
#         auxData = self.literal_eval(data)
#         if self.stock_id is not None and self.stock_id > 0:
#             stockStream = StockStream()
#             stockStream.stock_id = self.stock_id
#             stockStream.screenName = auxData['user']['screen_name']
#             stockStream.source = auxData['source']
#             stockStream.streamId = auxData['id_str']
#             stockStream.text = auxData['text']
#             stockStream.userId = auxData['user']['id_str']
#             stockStream.userName = auxData['user']['name']
#             stockStream.timestamp = str(auxData['timestamp_ms'])
#             stockStream.save()
#         return True
#
#     def on_error(self, status):
#         return status
#
#     def literal_eval(self, node_or_string):
#         """
#         Safely evaluate an expression node or a string containing a Python
#         expression.  The string or node provided may only consist of the following
#         Python literal structures: strings, numbers, tuples, lists, dicts, booleans,
#         and None.
#         """
#         _safe_names = {'None': None, 'True': True, 'False': False, 'null': None, 'true': True, 'false':False}
#         if isinstance(node_or_string, basestring):
#             node_or_string = parse(node_or_string, mode='eval')
#         if isinstance(node_or_string, Expression):
#             node_or_string = node_or_string.body
#
#         def _convert(node):
#             if isinstance(node, Str):
#                 return node.s
#             elif isinstance(node, Num):
#                 return node.n
#             elif isinstance(node, Tuple):
#                 return tuple(map(_convert, node.elts))
#             elif isinstance(node, List):
#                 return list(map(_convert, node.elts))
#             elif isinstance(node, Dict):
#                 return dict((_convert(k), _convert(v)) for k, v
#                             in zip(node.keys, node.values))
#             elif isinstance(node, Name):
#                 if node.id in _safe_names:
#                     return _safe_names[node.id]
#             elif isinstance(node, BinOp) and \
#                  isinstance(node.op, (Add, Sub)) and \
#                  isinstance(node.right, Num) and \
#                  isinstance(node.right.n, complex) and \
#                  isinstance(node.left, Num) and \
#                  isinstance(node.left.n, (int, long, float)):
#                 left = node.left.n
#                 right = node.right.n
#                 if isinstance(node.op, Add):
#                     return left + right
#                 else:
#                     return left - right
#             raise ValueError('malformed string')
#         return _convert(node_or_string)
#===================================================================================================
