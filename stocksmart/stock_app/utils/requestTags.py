# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''
CONTACTS_TAG = 'contacts'

DATE_FROM = 'dateFrom'
DATE_TO = 'dateTo'

ERROR_TAG = 'error'

GROUP_TAG = 'group'

INDEX_NAME_TAG = 'indexName'
ID_TAG = 'id'

NAME_TAG = 'name'

OWNER_TAG = 'owner'

PORTFOLIO_NAME_TAG = 'name'
PRICE_PAYED = 'pricePayed'

QUANTITY_OWNED = 'quantityOwned'
QUERY_TAG = 'q'

MESSAGE_TAG = 'message'

NATIONAL_PHONE_USER_TAG = 'nationalNumber'

STOCK_TAG = 'stock'
STOCKS_TAG = 'stocks'
SUCCESS_TAG = 'success'
STOCKS_EXTENDED = 'stocksExtended'

TYPE_TAG = 'type'

USERS_TAG = 'users'
