# -*- coding: utf-8 -*-
'''
Created on Aug 28, 2015

@author: rtorres
'''
from rest_framework import viewsets

from stock_app.models.stockNewsModel import StockNewsModel
from stock_app.serializers.stockNewsSerializers import StockNewsSerializer


class StockNewsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    @precondition: To use all methods you must be authenticated.

    @function GET: returns the specific news to
    """
    serializer_class = StockNewsSerializer
    stockId = None

    def get_queryset(self):
        return StockNewsModel.objects.filter(stock_id=self.stockId).order_by('-pubDate')[:20]

    def list(self, request, *args, **kwargs):
        self.stockId = kwargs.get('stockId', None)
        return viewsets.ReadOnlyModelViewSet.list(self, request, *args, **kwargs)
