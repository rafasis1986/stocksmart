# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''

import logging

from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import transaction
from django.db.utils import IntegrityError
from rest_framework import viewsets, status
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
import rest_framework

from config.utils.customExceptions import SerializerException
from config.utils.errorMessages import MSG_DUPICATE_PORTFOLIO, MSG_DEFAULT_PROJECT_ERROR
from config.utils.errorResponses import LogicalErrorResponse, TechnichalErrorResponse
from login.utils.constants import GROUP_PRIVATE
from stock_app.models.portfolioModel import Portfolio
from stock_app.models.stockModel import Stock
from stock_app.serializers.portfolioSerializers import PortfolioSerializer, PortfolioExtendedSerializer
from stock_app.utils.errorResponses import PortfolioDuplicatedError, MissedArgumentError
from stock_app.utils.exceptions import MissedArgumentException, NoStockGoogleException
from stock_app.utils.functions.stocks import initStocksInfo
from stock_app.utils.requestTags import STOCKS_TAG, GROUP_TAG, PORTFOLIO_NAME_TAG, OWNER_TAG, ID_TAG
from stock_app.utils.responseMessages import MSG_SYMBOL_STOCK_MISSED
from stock_app.utils.tools.errors import raiser


logger = logging.getLogger(__name__)


class PortfolioViewSet(viewsets.ModelViewSet):
    """
    @precondition: To use all methods you must be authenticated.

    @function GET: returns the details from the index.
    """
    serializer_class = PortfolioSerializer

    def get_queryset(self):
        return Portfolio.objects.filter(owner__id=self.request.user.id).order_by('pk')

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            stocks = list()
            stocksNames = list()
            if request.data.get(STOCKS_TAG) and type(request.data.get(STOCKS_TAG)) is list and \
                    len(request.data.get(STOCKS_TAG)) > 0:
                stocksNames = [str(item).upper().strip() for item in request.data.get(STOCKS_TAG)]
            else:
                raise MissedArgumentException(STOCKS_TAG)
            groupName = request.data.get(GROUP_TAG).upper() if request.data.get(GROUP_TAG) \
                else GROUP_PRIVATE
            if not request.data.get(PORTFOLIO_NAME_TAG, False):
                raiser(MissedArgumentException(PORTFOLIO_NAME_TAG))
            request.DATA.update({OWNER_TAG: request.user.pk})
            for item in stocksNames:
                s, auxBool = Stock.objects.get_or_create(symbol=item)
                stocks.append(s)
            response = viewsets.ModelViewSet.create(self, request, *args, **kwargs)
            if type(response) is Response and response.status_code is status.HTTP_201_CREATED:
                portfolio = Portfolio.objects.get(id=response.data.get(ID_TAG))
                portfolio.addStocks(stocks)
                group = Group.objects.get(name=groupName)
                portfolio.groups.add(group)
                serializer = PortfolioSerializer(portfolio)
                response = Response(serializer.data, status=status.HTTP_201_CREATED)
                initStocksInfo(stocks)
        except IntegrityError as e:
            response = Response(PortfolioDuplicatedError(MSG_DUPICATE_PORTFOLIO).__dict__, status=status.HTTP_409_CONFLICT)
        except MissedArgumentException as e:
            response = Response(MissedArgumentError(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except ParseError as e:
            response = Response(LogicalErrorResponse(e.detail).__dict__, status=status.HTTP_409_CONFLICT)
        except ValidationError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except rest_framework.exceptions.ValidationError as e:
            e.message = str(e.detail.get('non_field_errors')[0])
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(PortfolioDuplicatedError(MSG_DUPICATE_PORTFOLIO).__dict__, status=status.HTTP_409_CONFLICT)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse(MSG_DEFAULT_PROJECT_ERROR).__dict__, status=status.HTTP_400_BAD_REQUEST)
        return response

    def retrieve(self, request, pk):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            if pk is not None:
                portfolio = Portfolio.objects.get(id=pk)
                if portfolio.owner == request.user:
                    serializer = PortfolioExtendedSerializer(portfolio)
                    response = Response(serializer.data, status=status.HTTP_200_OK)
        except Portfolio.DoesNotExist as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response({"error": e.message}, status=status.HTTP_400_BAD_REQUEST)
        return response

    @transaction.atomic
    def destroy(self, request, pk):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            instance = Portfolio.objects.get(id=pk, ownerId=request.user.id)
            self.perform_destroy(instance)
            response = Response(status=status.HTTP_200_OK)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse(MSG_DEFAULT_PROJECT_ERROR).__dict__,
                                status=status.HTTP_400_BAD_REQUEST)
        return response

    @transaction.atomic
    def update(self, request, pk):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            stocksNames = request.data.get(STOCKS_TAG) if type(request.data.get(STOCKS_TAG)) is list and len(request.data.get(STOCKS_TAG)) > 0 else raiser(MissedArgumentException(STOCKS_TAG))
            instance = Portfolio.objects.get(id=pk, owner=request.user)
            stocksNames = [str(item).upper().strip() for item in stocksNames]
            stocks = list()
            for symbol in stocksNames:
                s, auxBool = Stock.objects.get_or_create(symbol=symbol)
                stocks.append(s)
            instance.addStocks(stocks)
            serializer = PortfolioSerializer(instance)
            response = Response(serializer.data, status=status.HTTP_200_OK)
            initStocksInfo(stocks)
        except MissedArgumentException as e:
            response = Response(MissedArgumentError(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except NoStockGoogleException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_404_NOT_FOUND)
        except SerializerException as e:
            response = Response(TechnichalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except ObjectDoesNotExist as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except ParseError as e:
            response = Response(LogicalErrorResponse(e.detail).__dict__, status=status.HTTP_409_CONFLICT)
        except ValidationError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_400_BAD_REQUEST)
        return response

    @transaction.atomic
    def remove(self, request, pk, symbol):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            symbol = symbol.upper().strip() if symbol else raiser(MissedArgumentException(MSG_SYMBOL_STOCK_MISSED))
            instance = Portfolio.objects.get(id=pk, owner=request.user)
            stock = Stock.objects.get(symbol=symbol)
            instance.stocks.remove(stock)
            serializer = PortfolioExtendedSerializer(instance)
            response = Response(serializer.data, status=status.HTTP_200_OK)
        except SerializerException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except ObjectDoesNotExist as e:
            response = Response(LogicalErrorResponse().__dict__, status=status.HTTP_409_CONFLICT)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_400_BAD_REQUEST)
        return response
