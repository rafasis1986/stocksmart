# -*- coding: utf-8 -*-
'''
Created on Aug 28, 2015

@author: rtorres
'''
from django.contrib.auth.models import Group
from rest_framework import viewsets

from login_stockbeat.utils.constants import GROUP_FEATURED
from stock_app.serializers.portfolioSerializers import PortfolioSerializer


class FeaturedPortfolioViewSet(viewsets.ReadOnlyModelViewSet):
    """
    @precondition: To use all methods you must be authenticated.

    @function GET: returns the portfolio lists from FEATURED group.
    """
    serializer_class = PortfolioSerializer

    def get_queryset(self):
        return Group.objects.get(name=GROUP_FEATURED).portfolio_set.all()
