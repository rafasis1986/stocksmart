# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''
import logging

from django.db import transaction
from django.db.utils import IntegrityError
from phonenumbers.phonenumberutil import NumberParseException
from rest_framework import viewsets, status
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.status import HTTP_409_CONFLICT
import phonenumbers

from config.utils.errorResponses import LogicalErrorResponse, TechnichalErrorResponse
from login.utils.constants import STATUS_SENT
from login.utils.errorResponses import PhoneFormatError
from login.utils.exceptions import InvalidPhoneError
from login.utils.functions.phones import formatPhoneToE164
from stock_app.models.contactModel import ContactModel
from stock_app.models.stockModel import Stock
from stock_app.models.stockTipsModel import StockTipsModel
from stock_app.models.userTipModel import UserTipModel
from stock_app.serializers.stockTipSerializers import StockTipSerializer, StockTipSentSerializer, \
    StockTipReceivedSerializer
from stock_app.utils.constants import TIP_TYPE_BUY, TIP_TYPE_SELL, TIP_TYPE_HOLD, STATUS_RECEIVED
from stock_app.utils.errorMessages import MSG_TIP_DONT_EXIST_ERROR, MSG_USER_TIP_EXIST_ERROR
from stock_app.utils.errorResponses import MissedArgumentError, StockDontExistError
from stock_app.utils.exceptions import MissedArgumentException, WrongTypeTipsException, \
    MissedContactNameException, MissedContactPhoneException, UserDontExistException, \
    StockDontExistException
from stock_app.utils.exceptions.tipExceptions import UserDontExistContactException, \
    UserPreviouslySentimentException, UserPreviouslySenTipException
from stock_app.utils.functions.stocks import findStockModel
from stock_app.utils.requestTags import STOCK_TAG, TYPE_TAG, CONTACTS_TAG, ERROR_TAG
from stock_app.utils.responseMessages import MSG_PHONE_ERROR_TIPS, MSG_ERROR_QUERY_TYPE
from stock_app.utils.tools.errors import raiser


logger = logging.getLogger(__name__)


class StockTipViewSet(viewsets.ModelViewSet):
    """
    @precondition: To use all methods you must be authenticated.

    @function <strong>POST</strong>: send stocks tips to other users
    """
    serializer_class = StockTipSerializer

    def get_queryset(self):
        return StockTipsModel.objects.filter(ownerId=self.request.user.id).order_by('-dateSent')

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        name = ''
        phone = ''
        stockName = None
        stock = None
        try:
            stockName = str(request.data.get(STOCK_TAG)) if request.data.get(STOCK_TAG) else \
                raiser(MissedArgumentException(STOCK_TAG))
            stockName = stockName.strip().upper()
            answer = findStockModel(stockName)
            if answer.get(STOCK_TAG).__class__ is Stock:
                stock = answer.get(STOCK_TAG)
            elif answer.get(ERROR_TAG) and answer.get(ERROR_TAG).__class__ is StockDontExistError:
                raise StockDontExistException(argument=stockName)
            typeTip = request.data.get(TYPE_TAG) if request.data.get(TYPE_TAG) else TIP_TYPE_BUY
            if typeTip not in [TIP_TYPE_BUY, TIP_TYPE_SELL, TIP_TYPE_HOLD]:
                raise WrongTypeTipsException()
            contacts = list()
            if type(request.data.get(CONTACTS_TAG)) is list and len(request.data.get(CONTACTS_TAG)) > 0:
                contacts = request.data.get(CONTACTS_TAG)
            else:
                raise MissedArgumentException(CONTACTS_TAG)
            smsTip = StockTipsModel()
            smsTip.owner = request.user
            smsTip.type = typeTip
            smsTip.stock = stock
            smsTip.save()
            for item in contacts:
                name = item.get('name', None)
                if name is None:
                    raise MissedContactNameException()
                elif type(name) is unicode:
                    name = name.encode("ascii", "ignore")
                phone = item.get('phone', None)
                if phone is None:
                    raise MissedContactPhoneException()
                elif type(phone) is unicode:
                    phone = phone.encode("ascii", "ignore")
                phone = formatPhoneToE164(phone)
                if not phone[1:].isdigit() or len(phone) < 5:
                    raise InvalidPhoneError()
                else:
                    phonenumbers.parse(phone, None)
                contact = ContactModel()
                contact.name = name
                contact.phone = phone
                contact.tips = smsTip
                contact.save()
            serializer = StockTipSentSerializer(smsTip)
            response = Response(serializer.data, status=status.HTTP_201_CREATED)
        except NumberParseException as e:
            response = Response(PhoneFormatError(e.message).__dict__, status=HTTP_409_CONFLICT)
        except MissedContactNameException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except MissedContactPhoneException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except InvalidPhoneError as e:
            message = MSG_PHONE_ERROR_TIPS % (phone, name)
            response = Response(PhoneFormatError(message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except MissedArgumentException as e:
            response = Response(MissedArgumentError(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except UserDontExistException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_404_NOT_FOUND)
        except StockDontExistError as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except StockDontExistException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except ParseError as e:
            response = Response(LogicalErrorResponse(e.detail).__dict__, status=status.HTTP_409_CONFLICT)
        except WrongTypeTipsException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except UserPreviouslySenTipException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except Exception as e:
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_400_BAD_REQUEST)
        return response

    def list(self, request):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            queryType = self.request.query_params.get(TYPE_TAG, None)
            national = str(request.user.phone.national_number)
            if queryType == STATUS_RECEIVED:
                contacts = ContactModel.objects.filter(phone__contains=national)
                tips = list()
                for c in contacts:
                    if c.tips.status == STATUS_SENT:
                        tips.append(c.tips)
                serializer = StockTipReceivedSerializer(tips, many=True)
                # {NATIONAL_PHONE_USER_TAG: national},
                response = Response(serializer.data)
            elif queryType == STATUS_SENT:
                serializer = StockTipSentSerializer(request.user.stocktipsmodel_set.all().
                                                    order_by('-dateCreated'), many=True)
                response = Response(serializer.data, status=status.HTTP_200_OK)
            else:
                response = Response(LogicalErrorResponse(message=MSG_ERROR_QUERY_TYPE).__dict__,
                                    status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_400_BAD_REQUEST)
        return response

    def sentiment(self, request, tipId):
        response = Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            sentiment = request.data.get(TYPE_TAG) if request.data.get(TYPE_TAG) in [TIP_TYPE_BUY, TIP_TYPE_SELL, TIP_TYPE_HOLD] else raiser(MissedArgumentException(TYPE_TAG))
            tip = StockTipsModel.objects.get(id=tipId, status=STATUS_SENT)
            national = str(request.user.phone.national_number)
            contacts = ContactModel.objects.filter(tips=tip, phone__contains=national).order_by('id')
            if len(contacts) == 0:
                raise UserDontExistContactException()
            UserTipModel(user=request.user, tip=tip, sentiment=sentiment).save()
            serializer = StockTipReceivedSerializer(tip)
            response = Response(serializer.data, status=status.HTTP_200_OK)
        except UserDontExistContactException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_401_UNAUTHORIZED)
        except UserPreviouslySentimentException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_409_CONFLICT)
        except MissedArgumentException as e:
            response = Response(LogicalErrorResponse(e.message).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except StockTipsModel.DoesNotExist as e:
            response = Response(LogicalErrorResponse(MSG_TIP_DONT_EXIST_ERROR).__dict__, status=status.HTTP_404_NOT_FOUND)
        except IntegrityError as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse(MSG_USER_TIP_EXIST_ERROR).__dict__, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.critical('%s, %s' % (type(e), e.message))
            response = Response(TechnichalErrorResponse().__dict__, status=status.HTTP_400_BAD_REQUEST)
        return response
