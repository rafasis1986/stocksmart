# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from rest_framework import viewsets
from rest_framework.response import Response

from stock_app.models.stockCatalogModel import StockCatalogModel
from stock_app.serializers.stockCatalogSerializers import StockCatalogSerializer
from stock_app.utils.requestTags import QUERY_TAG


class StockCatalogViewSet(viewsets.ModelViewSet):

    def list(self, request):
        response = Response({})
        queryParam = self.request.query_params.get(QUERY_TAG, None)
        if queryParam is not None:
            queryParam = queryParam.upper().strip()
            if len(queryParam) > 1:
                queryset = StockCatalogModel.objects.filter(
                    symbol__startswith=queryParam).order_by('symbol')
                if not queryset.exists():
                    queryset = StockCatalogModel.objects.filter(
                        symbol__contains=queryParam).order_by('symbol')
                    if not queryset.exists():
                        queryset = StockCatalogModel.objects.filter(
                            nameUpper__contains=queryParam).order_by('symbol')
                serializer = StockCatalogSerializer(queryset[:100], many=True)
                response = Response(serializer.data)
        return response
