from django.contrib import admin

from stock_app.models.contactModel import ContactModel
from stock_app.models.indexModel import IndexModel
from stock_app.models.intentTipsModel import IntentTipsModel
from stock_app.models.portfolioModel import Portfolio
from stock_app.models.stockCatalogModel import StockCatalogModel
from stock_app.models.stockHistoryModel import StockHistory
from stock_app.models.stockModel import Stock
from stock_app.models.stockNewsModel import StockNewsModel
from stock_app.models.stockTipsModel import StockTipsModel
from stock_app.models.userTipModel import UserTipModel
from stock_app.models.yahooSymbolModel import YahooSymbolModel


admin.site.register(Stock)
admin.site.register(IndexModel)
admin.site.register(Portfolio)
admin.site.register(StockTipsModel)
admin.site.register(IntentTipsModel)
admin.site.register(ContactModel)
admin.site.register(StockHistory)
admin.site.register(StockCatalogModel)
admin.site.register(UserTipModel)
admin.site.register(StockNewsModel)
admin.site.register(YahooSymbolModel)
