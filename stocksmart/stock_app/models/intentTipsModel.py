# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
from django.db import models


class IntentTipsModel(models.Model):
    id = models.SlugField(primary_key=True, unique=True, editable=False)
    intent = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = 'intent tips'
        db_table = 'intent_tips'

    def __unicode__(self):
        return u"%s: %s" % (self.id, self.intent)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.intent += 1
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
