# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
import time

from django.db import models


class StockCatalogModel(models.Model):
    symbol = models.CharField(max_length=32, db_index=True, unique=True)
    name = models.CharField(max_length=256, null=True)
    nameUpper = models.CharField(max_length=256, null=True, db_index=True)
    dateCreated = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'stock catalog'
        db_table = 'catalog'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.symbol = self.symbol.strip().upper()
        self.name = self.name.strip()
        self.nameUpper = self.name.upper()
        if not self.pk:
            self.dateCreated = long(time.time())
        result = models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
        return result

    def __unicode__(self):
        return u"%s " % (self.symbol)
