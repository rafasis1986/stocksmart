# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
import time

from django.db import models

from config.models import AbstractHashModel
from stock_app.models.validators.stockValidators import validateGoogleSymbol
from stock_app.utils.functions.cache import findCurrentPriceCache
from stock_app.utils.tools import stockretriever


class Stock(AbstractHashModel):
    symbol = models.CharField(max_length=32, db_index=True)
    name = models.CharField(max_length=32, null=True)
    index = models.CharField(max_length=32, blank=True, default='')
    dateLastConsulted = models.PositiveIntegerField(default=0)
    openPrice = models.DecimalField(max_digits=10, decimal_places=2, default=0, null=True)
    closePrice = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    fiftyDayAvg = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    twoHundredDayAvg = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    previousDayClose = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    yearHigh = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    yearLow = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    streaming = models.BooleanField(default=False)
    hasHistory = models.BooleanField(default=False)
    hasRecentHistory = models.BooleanField(default=False)
    isIndex = models.BooleanField(default=False, db_index=True)
    nextEarning = models.CharField(max_length=64, default='', blank=True, null=True)
    eps = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    pe = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    nextEarningTime = models.PositiveIntegerField(default=0)
    yahooSymbol = models.CharField(max_length=32, unique=True, db_index=True)
    sector = models.CharField(max_length=128, default='n/a')
    industry = models.CharField(max_length=128, default='n/a')
    lastTradePrice = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    class Meta:
        verbose_name = 'stock'
        verbose_name_plural = 'stocks'
        db_table = 'stock'
        unique_together = ('index', 'symbol')

    def clean(self):
        self.symbol = str(self.symbol).upper().strip()
        if not self.yahooSymbol:
            aux = validateGoogleSymbol(self.symbol)
            if not self.isIndex:
                self.lastTradePrice = float(aux[0].get('LastTradePrice', '0'))
            self.yahooSymbol = self.symbol.replace('.', '-')
            if not self.isIndex:
                self.getStatistics()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Stock, self).save()
        self.clean()
        models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using,
            update_fields=update_fields)
        return self

    def __unicode__(self):
        return u"%s " % (self.name)

    @property
    def compositeName(self):
        response = ''
        if len(self.index) > 0:
            response += self.index + ':'
        response += self.symbol
        return response

    @property
    def currentPrice(self):
        response = None
        if not self.isIndex:
            try:
                aux = findCurrentPriceCache(self.symbol)
                response = "%0.2f" % float(aux)
            except Exception:
                response = "%0.2f" % float(self.lastTradePrice)
        else:
            response = "%0.2f" % float(0)
        return response

    def getStatistics(self):
        if not self.isIndex:
            item = stockretriever.get_current_info([self.yahooSymbol],
                columnsToRetrieve=['Symbol', 'Name', 'Open', 'FiftydayMovingAverage',
                'TwoHundreddayMovingAverage', 'PreviousClose', 'YearHigh', 'YearLow'])
            self.name = '...' if item['Name'] is None else str(item['Name'])
            self.fiftyDayAvg = 0 if item['FiftydayMovingAverage'] is None else float(item['FiftydayMovingAverage'])
            self.twoHundredDayAvg = 0 if item['TwoHundreddayMovingAverage'] is None else float(item['TwoHundreddayMovingAverage'])
            self.previousDayClose = 0 if item['PreviousClose'] is None else float(item['PreviousClose'])
            self.yearHigh = 0 if item['YearHigh'] is None else float(item['YearHigh'])
            self.yearLow = 0 if item['YearLow'] is None else float(item['YearLow'])
            self.openPrice = 0 if item['Open'] is None else float(item['Open'])
        return self

    def setClosePrice(self):
        item = stockretriever.get_current_info([self.yahooSymbol], columnsToRetrieve=['LastTradePriceOnly'])
        if item['LastTradePriceOnly'] is not None:
            self.closePrice = float(item['LastTradePriceOnly'])
            self.dateLastConsulted = long(time.time())
        return self
