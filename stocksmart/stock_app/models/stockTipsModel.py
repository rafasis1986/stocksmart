# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
import logging
import time

from django.contrib.auth.models import User
from django.db import models

from config.models import AbstractHashModel
from stock_app.models.stockModel import Stock
from stock_app.utils.constants import STATUS_WAITING, TIP_TYPE_BUY, TIP_TYPE_SELL
import datetime
from stock_app.utils.exceptions.tipExceptions import UserPreviouslySenTipException


logger = logging.getLogger(__name__)


class StockTipsModel(AbstractHashModel):
    owner = models.ForeignKey(User, db_index=True)
    dateCreated = models.PositiveIntegerField(db_index=True)
    dateSent = models.PositiveIntegerField(default=0, db_index=True)
    status = models.CharField(max_length=16, default=STATUS_WAITING)
    message = models.TextField(blank=True, null=True)
    type = models.CharField(max_length=16, default=TIP_TYPE_BUY)
    tipPrice = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    stock = models.ForeignKey(Stock, db_index=True)

    def __unicode__(self):
        return u"%s: %s" % (self.pk, self.owner.username)

    class Meta:
        verbose_name = 'stock tip'
        verbose_name_plural = 'stock tips'
        db_table = "stock_tip"
        ordering = ('dateCreated', 'dateSent', )

    def clean(self):
        self.validateUserSentToday()
        return True

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id and self.clean():
            super(StockTipsModel, self).save()
            self.dateCreated = long(time.time())
            self.tipPrice = self.stock.currentPrice
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    @property
    def performance(self):
        a = float(self.stock.currentPrice)
        b = float(self.tipPrice)
        difference = a - b
        if self.type == TIP_TYPE_SELL:
            difference *= -1
        return "%0.2f" % difference

    @property
    def performanceInPercentage(self):
        difference = float(self.performance)
        result = difference * 100 / float(self.tipPrice)
        return "%0.2f" % (result)

    def validateUserSentToday(self):
        dt = datetime.date.today().timetuple()
        today = int(time.mktime(dt))
        try:
            StockTipsModel.objects.get(owner=self.owner, stock=self.stock, dateCreated__gte=today)
            raise UserPreviouslySenTipException(self.stock.symbol)
        except StockTipsModel.DoesNotExist:
            pass
