# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.db import models

from stock_app.models.stockModel import Stock


class StockHistory(models.Model):
    stock = models.ForeignKey(Stock)
    date = models.PositiveIntegerField(default=0, db_index=True)
    open = models.DecimalField(max_digits=11, decimal_places=3, default=0)
    close = models.DecimalField(max_digits=11, decimal_places=3, default=0)
    volume = models.PositiveIntegerField(default=0)
    high = models.DecimalField(max_digits=11, decimal_places=3, default=0)
    low = models.DecimalField(max_digits=11, decimal_places=3, default=0)

    class Meta:
        verbose_name = 'stock history'
        verbose_name_plural = 'stock histories'
        db_table = 'stock_history'
        unique_together = ('stock', 'date')
        index_together = ['stock', 'date']

    def validate_unique(self, *args, **kwargs):
        super(StockHistory, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter().exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS:
                        ('History with same date and stock already exist.',)
                    }
                )

    def __unicode__(self):
        return u"%s - %s -%s" % (self.stock.id, self.stock.compositeName, self.stock.isIndex)
