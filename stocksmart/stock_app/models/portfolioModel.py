# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
import time

from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.db import models

from config.models import AbstractHashModel
from stock_app.models.stockModel import Stock
from stock_app.utils.errorMessages import MSG_USER_PORTFOLIO_EXIST_ERROR


class Portfolio(AbstractHashModel):
    owner = models.ForeignKey(User, db_index=True)
    name = models.CharField(max_length=64, db_index=True)
    stocks = models.ManyToManyField(Stock)
    groups = models.ManyToManyField(Group)
    dateCreated = models.PositiveIntegerField(default=0, db_index=True)
    dateUpdated = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = 'Portfolio'
        verbose_name_plural = 'Portfolios'
        db_table = 'potfolio'
        unique_together = (('owner', 'name'),)
        index_together = ['owner', 'name']

    def __unicode__(self):
        return u"%s - %s" % (self.name, self.owner.username)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Portfolio, self).save()
        self.name = self.name.strip().upper()
        if self.dateCreated == 0:
            self.dateCreated = long(time.time())
        else:
            self.dateUpdated = long(time.time())
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def validate_unique(self, *args, **kwargs):
        super(Portfolio, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter().exists():
                raise ValidationError({NON_FIELD_ERRORS: (MSG_USER_PORTFOLIO_EXIST_ERROR,)})

    def removeAllStocks(self):
        for stock in self.stocks.all():
            self.stocks.remove(stock)

    def addStocks(self, stocks=[]):
        for stock in stocks:
            if type(stock) is Stock:
                self.stocks.add(stock)
