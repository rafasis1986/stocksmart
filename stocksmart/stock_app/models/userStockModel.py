# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
import time

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.db import models

from config.models import AbstractHashModel
from stock_app.models.stockModel import Stock


class UserStock(AbstractHashModel):
    user = models.ForeignKey(User, db_index=True)
    stock = models.ForeignKey(Stock, db_index=True)
    dateAfiliated = models.PositiveIntegerField(default=0)
    pricePayed = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    quantityOwned = models.PositiveIntegerField(default=0)
    datePurchased = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'user stock'
        verbose_name_plural = 'user stocks'
        db_table = 'user_stock'
        unique_together = ('user', 'stock')
        index_together = ['user', 'stock']

    def __unicode__(self):
        return u"%s - %s" % (self.user.username, self.stock.compositeName)

    def validate_unique(self, *args, **kwargs):
        super(UserStock, self).validate_unique(*args, **kwargs)
        if not self.id:
            if self.__class__.objects.filter().exists():
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS:
                        ('User with same Stock already exists.',)
                    }
                )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(UserStock, self).save()
        if force_update is False and update_fields is None:
            self.dateAfiliated = long(time.time())
        return models.Model.save(self, force_insert=force_insert, force_update=force_update,
                                 using=using, update_fields=update_fields)
