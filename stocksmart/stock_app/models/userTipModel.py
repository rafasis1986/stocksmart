# -*- coding: utf-8 -*-
'''
Created on Aug 26, 2015

@author: rtorres
'''
from django.db import models
from stock_app.models.stockTipsModel import StockTipsModel
from django.contrib.auth.models import User
import time


class UserTipModel(models.Model):
    user = models.ForeignKey(User, db_index=True)
    tip = models.ForeignKey(StockTipsModel, db_index=True)
    sentiment = models.CharField(max_length=16, null=True)
    date = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'user tip'
        db_table = 'user_tip'
        unique_together = ('user', 'tip')
        index_together = ['user', 'tip']

    def __unicode__(self):
        return u"%s: %s" % (self.user.username, self.tip)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.date = long(time.time())
        return models.Model.save(self, force_insert=force_insert, force_update=force_update,
            using=using, update_fields=update_fields)
