# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
from django.db import models

from config.models import AbstractHashModel
from stock_app.models.stockTipsModel import StockTipsModel


class ContactModel(AbstractHashModel):
    name = models.CharField(max_length=256)
    phone = models.CharField(max_length=16, db_index=True)
    tips = models.ForeignKey(StockTipsModel)
    status = models.TextField(default='')

    class Meta:
        verbose_name = 'contact tip'
        db_table = 'contact_tips'

    def __unicode__(self):
        return u"%s: %s" % (self.name, self.phone)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(ContactModel, self).save()
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
