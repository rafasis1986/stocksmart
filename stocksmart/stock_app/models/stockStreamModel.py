# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
#===================================================================================================
# class StockStream(AbstractHashModel):
#     streamId = models.CharField(max_length=64, db_index=True)
#     userId = models.CharField(max_length=64)
#     stock = models.ForeignKey(Stock)
#     text = models.CharField(max_length=256)
#     source = models.CharField(max_length=256)
#     userName = models.CharField(max_length=64, db_index=True)
#     screenName = models.CharField(max_length=128)
#     date = models.PositiveIntegerField(default=0)
#
#     class Meta:
#         verbose_name = 'stock stream'
#         verbose_name_plural = 'stocks stream'
#         db_table = 'stock_stream'
#
#     def __unicode__(self):
#         return u"%s - %s" % (self.id, self.stock.compositeName)
#
#     @property
#     def date(self):
#         return datetime.fromtimestamp(long(self.timestamp))
#===================================================================================================
