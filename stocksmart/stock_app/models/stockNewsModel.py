# -*- coding: utf-8 -*-
'''
Created on Sep 11, 2015

@author: rtorres
'''
import time

from django.db import models

from stock_app.models.stockModel import Stock


class StockNewsModel(models.Model):
    desc = models.TextField(null=True)
    yahooSymbol = models.CharField(max_length=16, db_index=True)
    title = models.CharField(max_length=256, db_index=True)
    pubDate = models.PositiveIntegerField(default=0)
    link = models.URLField(db_index=True)
    stock = models.ForeignKey(Stock)
    dateCreated = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = False
        verbose_name = 'stock news'
        verbose_name_plural = 'stocks news'
        db_table = 'stock_news'
        ordering = ('stock', 'pubDate', 'link', 'dateCreated',)
        unique_together = ('link', 'stock', 'title')

    def __unicode__(self):
        return u"%s - %s" % (self.stock, self.title)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            super(StockNewsModel, self).save()
            self.dateCreated = long(time.time())
        models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using,
            update_fields=update_fields)
        return self
