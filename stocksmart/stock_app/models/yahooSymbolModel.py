# -*- coding: utf-8 -*-
'''
Created on Oct 22, 2015

@author: rtorres
'''
from django.db import models


class YahooSymbolModel(models.Model):
    currentSymbol = models.CharField(max_length=16, db_index=True, unique=True)
    newSymbol = models.CharField(max_length=16, db_index=True, default='')

    class Meta:
        verbose_name = 'yahoo symbol'
        db_table = 'yahoo_symbol'

    def __unicode__(self):
        return u"%s - %s" % (self.currentSymbol, self.newSymbol)
