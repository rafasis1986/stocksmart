# -*- coding: utf-8 -*-
'''
Created on Aug 27, 2015

@author: rtorres
'''
from googlefinance import getQuotes

from stock_app.utils.exceptions.genericExceptions import ValidationException
from stock_app.utils.exceptions.stockExceptions import DontExistGoogleSymbolException


def validateGoogleSymbol(value):
    response = None
    try:
        value = str(value).upper()
        response = getQuotes(value)
    except IOError:
        raise DontExistGoogleSymbolException(value)
    except Exception as e:
        raise ValidationException(type(e))
    return response
