# -*- coding: utf-8 -*-
'''
Created on Aug 21, 2015

@author: rtorres
'''
import time

from django.db import models
from yahoo_finance import Share

from config.models import AbstractHashModel
from stock_app.models.stockModel import Stock
from stock_app.utils.responseTags import INDEX


class IndexModel(AbstractHashModel):
    stock = models.OneToOneField(Stock, db_index=True, default=0)
    yahooSymbol = models.CharField(max_length=32, unique=True)
    googleSymbol = models.CharField(max_length=32, unique=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(IndexModel, self).save()
        self.stock.isIndex = True
        self.index = INDEX
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    class Meta:
        verbose_name = 'index'
        verbose_name_plural = 'indexes'
        db_table = 'index'

    def __unicode__(self):
        return u"%s - %s" % (self.id, self.stock.compositeName)

    def setClosePrice(self):
        index = Share(self.yahooSymbol)
        if index.data_set is not None:
            self.stock.closePrice = index.data_set['LastTradePriceOnly']
            self.stock.dateLastConsulted = long(time.time())
        return self.stock

    @property
    def compositeName(self):
        response = self.googleSymbol
        return response
