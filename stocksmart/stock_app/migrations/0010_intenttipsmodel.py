# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0009_auto_20150814_1447'),
    ]

    operations = [
        migrations.CreateModel(
            name='IntentTipsModel',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('intent', models.PositiveSmallIntegerField(default=0)),
            ],
            options={
                'db_table': 'intent_tip',
                'verbose_name': 'intent tip',
            },
        ),
    ]
