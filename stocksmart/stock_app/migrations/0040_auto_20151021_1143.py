# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0039_auto_20150915_1238'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='stocknewsmodel',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='stocknewsmodel',
            name='stock',
        ),
        migrations.DeleteModel(
            name='StockNewsModel',
        ),
    ]
