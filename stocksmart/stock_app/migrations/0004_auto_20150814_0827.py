# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0003_auto_20150813_1940'),
    ]

    operations = [
        migrations.AddField(
            model_name='stock',
            name='industry',
            field=models.CharField(default=b'n/a', max_length=128),
        ),
        migrations.AddField(
            model_name='stock',
            name='sector',
            field=models.CharField(default=b'n/a', max_length=128),
        ),
    ]
