# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0007_auto_20150814_1118'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactModel',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('name', models.CharField(max_length=256)),
            ],
            options={
                'db_table': 'contact_tip',
                'verbose_name': 'contact tip',
            },
        ),
        migrations.RemoveField(
            model_name='stocktipsmodel',
            name='users',
        ),
        migrations.AddField(
            model_name='stocktipsmodel',
            name='contacts',
            field=models.ManyToManyField(to='stock_app.ContactModel'),
        ),
    ]
