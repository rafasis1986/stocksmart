# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0012_contactmodel_phone'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='intenttipsmodel',
            options={'verbose_name': 'intent tips'},
        ),
        migrations.AlterModelTable(
            name='intenttipsmodel',
            table='intent_tips',
        ),
    ]
