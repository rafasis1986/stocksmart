# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0011_auto_20150818_0803'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactmodel',
            name='phone',
            field=models.CharField(default='+111111', max_length=16, db_index=True),
            preserve_default=False,
        ),
    ]
