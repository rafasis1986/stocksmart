# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0027_auto_20150824_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='stocktipsmodel',
            name='stock',
            field=models.ForeignKey(default='00000001', to='stock_app.Stock'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='stocktipsmodel',
            name='tipPrice',
            field=models.DecimalField(default=0, max_digits=10, decimal_places=2),
        ),
    ]
