# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0004_auto_20150814_0827'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockCatalogModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('symbol', models.CharField(unique=True, max_length=32, db_index=True)),
                ('name', models.CharField(max_length=256, null=True, db_index=True)),
                ('sector', models.CharField(default=b'n/a', max_length=128, db_index=True)),
                ('industry', models.CharField(default=b'n/a', max_length=128, db_index=True)),
                ('dateCreated', models.PositiveIntegerField(default=0)),
            ],
            options={
                'db_table': 'catalog',
                'verbose_name': 'stock catalog',
            },
        ),
    ]
