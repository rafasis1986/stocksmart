# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='IndexModel',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('yahooSymbol', models.CharField(unique=True, max_length=32)),
                ('googleSymbol', models.CharField(unique=True, max_length=32)),
            ],
            options={
                'db_table': 'index',
                'verbose_name': 'index',
                'verbose_name_plural': 'indexes',
            },
        ),
        migrations.CreateModel(
            name='Portfolio',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('name', models.CharField(max_length=64, db_index=True)),
                ('dateCreated', models.PositiveIntegerField(default=0, db_index=True)),
                ('dateUpdated', models.PositiveIntegerField(default=0, db_index=True)),
                ('groups', models.ManyToManyField(to='auth.Group')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'potfolio',
                'verbose_name': 'Portfolio',
                'verbose_name_plural': 'Portfolios',
            },
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('symbol', models.CharField(max_length=32, db_index=True)),
                ('name', models.CharField(max_length=32, null=True)),
                ('index', models.CharField(default=b'', max_length=32, blank=True)),
                ('dateLastConsulted', models.PositiveIntegerField(default=0)),
                ('openPrice', models.DecimalField(default=0, null=True, max_digits=10, decimal_places=2)),
                ('closePrice', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('fiftyDayAvg', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('twoHundredDayAvg', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('previousDayClose', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('yearHigh', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('yearLow', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('streaming', models.BooleanField(default=False)),
                ('hasHistory', models.BooleanField(default=False)),
                ('isIndex', models.BooleanField(default=False, db_index=True)),
                ('nextEarning', models.CharField(default=b'', max_length=64, null=True, blank=True)),
                ('eps', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('pe', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('nextEarningTime', models.PositiveIntegerField(default=0)),
                ('yahooSymbol', models.CharField(unique=True, max_length=32, db_index=True)),
            ],
            options={
                'db_table': 'stock',
                'verbose_name': 'stock',
                'verbose_name_plural': 'stocks',
            },
        ),
        migrations.CreateModel(
            name='UserStock',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('dateAfiliated', models.PositiveIntegerField(default=0)),
                ('pricePayed', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('quantityOwned', models.PositiveIntegerField(default=0)),
                ('datePurchased', models.PositiveIntegerField(default=0)),
                ('stock', models.ForeignKey(to='stock_app.Stock')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_stock',
                'verbose_name': 'user stock',
                'verbose_name_plural': 'user stocks',
            },
        ),
        migrations.AlterUniqueTogether(
            name='stock',
            unique_together=set([('index', 'symbol')]),
        ),
        migrations.AddField(
            model_name='portfolio',
            name='stocks',
            field=models.ManyToManyField(to='stock_app.Stock'),
        ),
        migrations.AddField(
            model_name='indexmodel',
            name='stock',
            field=models.OneToOneField(default=0, to='stock_app.Stock'),
        ),
        migrations.AlterUniqueTogether(
            name='userstock',
            unique_together=set([('user', 'stock')]),
        ),
        migrations.AlterIndexTogether(
            name='userstock',
            index_together=set([('user', 'stock')]),
        ),
        migrations.AlterUniqueTogether(
            name='portfolio',
            unique_together=set([('owner', 'name')]),
        ),
        migrations.AlterIndexTogether(
            name='portfolio',
            index_together=set([('owner', 'name')]),
        ),
    ]
