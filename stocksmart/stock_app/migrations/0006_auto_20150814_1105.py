# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0005_stockcatalogmodel'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='stockcatalogmodel',
            name='industry',
        ),
        migrations.RemoveField(
            model_name='stockcatalogmodel',
            name='sector',
        ),
    ]
