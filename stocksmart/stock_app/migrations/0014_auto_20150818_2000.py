# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0013_auto_20150818_0854'),
    ]

    operations = [
        migrations.AddField(
            model_name='stocktipsmodel',
            name='tipsPrice',
            field=models.DecimalField(default=0, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='stocktipsmodel',
            name='type',
            field=models.CharField(default=b'buy', max_length=16),
        ),
    ]
