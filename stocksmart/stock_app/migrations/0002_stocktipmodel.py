# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('stock_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockTipModel',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('ownerId', models.PositiveIntegerField(db_index=True)),
                ('dateCreated', models.PositiveIntegerField()),
                ('dateSent', models.PositiveIntegerField(default=0, db_index=True)),
                ('status', models.CharField(default=b'waiting', max_length=16)),
                ('message', models.TextField(null=True, blank=True)),
                ('stocks', models.ManyToManyField(to='stock_app.Stock')),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'stock_tip',
                'verbose_name': 'stock tip',
                'verbose_name_plural': 'stock tips',
            },
        ),
    ]
