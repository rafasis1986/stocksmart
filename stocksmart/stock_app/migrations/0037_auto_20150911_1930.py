# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0036_stock_lasttradeprice'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockNewsModel',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('desc', models.TextField(null=True)),
                ('title', models.CharField(max_length=256)),
                ('pubDate', models.PositiveIntegerField(default=0)),
                ('dateCreated', models.PositiveIntegerField(default=0)),
                ('link', models.URLField(db_index=True)),
                ('stock', models.ForeignKey(to='stock_app.Stock')),
            ],
            options={
                'ordering': ('pubDate', 'link', 'dateCreated'),
                'db_table': 'stock_news',
                'verbose_name': 'stock news',
                'verbose_name_plural': 'stocks news',
            },
        ),
        migrations.AlterUniqueTogether(
            name='stocknewsmodel',
            unique_together=set([('link', 'stock')]),
        ),
    ]
