# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0015_stocktipsmodel_owner'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='stocktipsmodel',
            name='ownerId',
        ),
        migrations.AlterField(
            model_name='stocktipsmodel',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
