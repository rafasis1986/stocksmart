# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('stock_app', '0030_contactmodel_sentiment'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserTipModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sentiment', models.CharField(max_length=16, null=True)),
                ('date', models.PositiveIntegerField()),
                ('tip', models.ForeignKey(to='stock_app.StockTipsModel')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_tip',
                'verbose_name': 'user tip',
            },
        ),
        migrations.AlterModelOptions(
            name='contactmodel',
            options={'verbose_name': 'contact tip'},
        ),
        migrations.AlterUniqueTogether(
            name='usertipmodel',
            unique_together=set([('user', 'tip')]),
        ),
        migrations.AlterIndexTogether(
            name='usertipmodel',
            index_together=set([('user', 'tip')]),
        ),
    ]
