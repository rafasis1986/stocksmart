# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0028_auto_20150824_1522'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tipspricemodel',
            name='stock',
        ),
        migrations.DeleteModel(
            name='TipsPriceModel',
        ),
    ]
