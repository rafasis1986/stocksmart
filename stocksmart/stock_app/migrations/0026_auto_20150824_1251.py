# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0025_contactmodel_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactmodel',
            name='status',
            field=models.TextField(default=b''),
        ),
    ]
