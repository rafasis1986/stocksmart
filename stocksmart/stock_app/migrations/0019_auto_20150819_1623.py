# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0018_auto_20150818_2047'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.PositiveIntegerField(default=0, db_index=True)),
                ('open', models.DecimalField(default=0, max_digits=11, decimal_places=3)),
                ('close', models.DecimalField(default=0, max_digits=11, decimal_places=3)),
                ('volume', models.PositiveIntegerField(default=0)),
                ('high', models.DecimalField(default=0, max_digits=11, decimal_places=3)),
                ('low', models.DecimalField(default=0, max_digits=11, decimal_places=3)),
                ('stock', models.ForeignKey(to='stock_app.Stock')),
            ],
            options={
                'db_table': 'stock_history',
                'verbose_name': 'stock history',
                'verbose_name_plural': 'stock histories',
            },
        ),
        migrations.AlterUniqueTogether(
            name='stockhistory',
            unique_together=set([('stock', 'date')]),
        ),
        migrations.AlterIndexTogether(
            name='stockhistory',
            index_together=set([('stock', 'date')]),
        ),
    ]
