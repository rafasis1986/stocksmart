# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0016_auto_20150818_2017'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipsPriceModel',
            fields=[
                ('id', models.SlugField(primary_key=True, serialize=False, editable=False, unique=True)),
                ('tipsPrice', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('stock', models.ForeignKey(to='stock_app.Stock')),
            ],
            options={
                'db_table': 'tips_price',
                'verbose_name': 'tips price',
                'verbose_name_plural': 'tips prices',
            },
        ),
        migrations.RemoveField(
            model_name='stocktipsmodel',
            name='stocks',
        ),
        migrations.RemoveField(
            model_name='stocktipsmodel',
            name='tipsPrice',
        ),
        migrations.AddField(
            model_name='tipspricemodel',
            name='tips',
            field=models.ForeignKey(to='stock_app.StockTipsModel'),
        ),
    ]
