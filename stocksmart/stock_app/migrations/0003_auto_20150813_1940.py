# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0002_stocktipmodel'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='StockTipModel',
            new_name='StockTipsModel',
        ),
        migrations.AlterModelOptions(
            name='stocktipsmodel',
            options={'verbose_name': 'stock tips', 'verbose_name_plural': 'stock tips'},
        ),
        migrations.AlterModelTable(
            name='stocktipsmodel',
            table='stock_tips',
        ),
    ]
