# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0037_auto_20150911_1930'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='stocktipsmodel',
            options={'ordering': ('dateCreated', 'dateSent'), 'verbose_name': 'stock tip', 'verbose_name_plural': 'stock tips'},
        ),
        migrations.AlterField(
            model_name='stocknewsmodel',
            name='title',
            field=models.CharField(max_length=256, db_index=True),
        ),
        migrations.AlterUniqueTogether(
            name='stocknewsmodel',
            unique_together=set([('link', 'stock', 'title')]),
        ),
    ]
