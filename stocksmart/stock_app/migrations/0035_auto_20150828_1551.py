# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0034_auto_20150827_1830'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='userstock',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='userstock',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='userstock',
            name='stock',
        ),
        migrations.RemoveField(
            model_name='userstock',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserStock',
        ),
    ]
