# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0010_intenttipsmodel'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contactmodel',
            options={'verbose_name': 'contact tips'},
        ),
        migrations.AlterModelTable(
            name='contactmodel',
            table='contact_tips',
        ),
    ]
