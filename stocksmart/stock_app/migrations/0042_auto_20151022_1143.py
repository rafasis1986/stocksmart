# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0041_auto_20151022_1043'),
    ]

    operations = [
        migrations.CreateModel(
            name='YahooSymbolModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('currentSymbol', models.CharField(unique=True, max_length=16, db_index=True)),
                ('newSymbol', models.CharField(default=b'', max_length=16, db_index=True)),
            ],
            options={
                'db_table': 'yahoo_symbol',
                'verbose_name': 'yahoo symbol',
            },
        ),
        migrations.AlterModelOptions(
            name='stocknewsmodel',
            options={'ordering': ('stock', 'pubDate', 'link', 'dateCreated'), 'verbose_name': 'stock news', 'verbose_name_plural': 'stocks news'},
        ),
    ]
