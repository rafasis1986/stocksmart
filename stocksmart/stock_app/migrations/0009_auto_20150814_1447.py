# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0008_auto_20150814_1441'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='stocktipsmodel',
            name='contacts',
        ),
        migrations.AddField(
            model_name='contactmodel',
            name='tips',
            field=models.ForeignKey(default=None, to='stock_app.StockTipsModel'),
            preserve_default=False,
        ),
    ]
