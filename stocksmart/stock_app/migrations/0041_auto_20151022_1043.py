# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0040_auto_20151021_1143'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockNewsModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('desc', models.TextField(null=True)),
                ('yahooSymbol', models.CharField(max_length=16, db_index=True)),
                ('title', models.CharField(max_length=256, db_index=True)),
                ('pubDate', models.PositiveIntegerField(default=0)),
                ('link', models.URLField(db_index=True)),
                ('dateCreated', models.PositiveIntegerField(default=0)),
                ('stock', models.ForeignKey(to='stock_app.Stock')),
            ],
            options={
                'ordering': ('pubDate', 'link', 'dateCreated'),
                'abstract': False,
                'verbose_name_plural': 'stocks news',
                'db_table': 'stock_news',
                'verbose_name': 'stock news',
            },
        ),
        migrations.AlterUniqueTogether(
            name='stocknewsmodel',
            unique_together=set([('link', 'stock', 'title')]),
        ),
    ]
