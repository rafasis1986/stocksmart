# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0024_delete_testmodel'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactmodel',
            name='status',
            field=models.CharField(default=b'', max_length=256),
        ),
    ]
