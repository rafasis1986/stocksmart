# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0006_auto_20150814_1105'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockcatalogmodel',
            name='nameUpper',
            field=models.CharField(max_length=256, null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='stockcatalogmodel',
            name='name',
            field=models.CharField(max_length=256, null=True),
        ),
    ]
