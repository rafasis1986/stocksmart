# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0038_auto_20150914_1146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stocktipsmodel',
            name='dateCreated',
            field=models.PositiveIntegerField(db_index=True),
        ),
    ]
