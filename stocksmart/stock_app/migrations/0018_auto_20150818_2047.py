# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0017_auto_20150818_2040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tipspricemodel',
            name='tips',
            field=models.ForeignKey(to='stock_app.StockTipsModel', null=True),
        ),
    ]
