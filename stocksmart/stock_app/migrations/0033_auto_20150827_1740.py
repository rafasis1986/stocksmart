# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import stock_app.models.validators.stockValidators


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0032_remove_contactmodel_sentiment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stock',
            name='symbol',
            field=models.CharField(db_index=True, max_length=32, validators=[stock_app.models.validators.stockValidators.validateGoogleSymbol]),
        ),
    ]
