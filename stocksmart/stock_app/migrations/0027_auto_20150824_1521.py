# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0026_auto_20150824_1251'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='stocktipsmodel',
            options={'verbose_name': 'stock tip', 'verbose_name_plural': 'stock tips'},
        ),
        migrations.RemoveField(
            model_name='tipspricemodel',
            name='tips',
        ),
        migrations.AlterModelTable(
            name='stocktipsmodel',
            table='stock_tip',
        ),
    ]
