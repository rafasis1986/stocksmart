# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0035_auto_20150828_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='stock',
            name='lastTradePrice',
            field=models.DecimalField(default=0, max_digits=10, decimal_places=2),
        ),
    ]
