# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0029_auto_20150824_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactmodel',
            name='sentiment',
            field=models.CharField(max_length=16, null=True),
        ),
    ]
