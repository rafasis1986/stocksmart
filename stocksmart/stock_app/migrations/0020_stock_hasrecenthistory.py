# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock_app', '0019_auto_20150819_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='stock',
            name='hasRecentHistory',
            field=models.BooleanField(default=False),
        ),
    ]
