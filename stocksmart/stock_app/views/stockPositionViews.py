# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
import time

from django.db import transaction
from django.http.response import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from stock_app.models.stockModel import Stock
from stock_app.models.userStockModel import UserStock
from stock_app.serializers.userStockSerializers import UserStockPositionSerializer
from stock_app.utils.requestTags import PRICE_PAYED, QUANTITY_OWNED, SUCCESS_TAG, ERROR_TAG


class StockPositionView(APIView):
    """
    @precondition: To use all methods you must be authenticated and the stock_app have been affiliated
    to account user previously.

    @function <strong>GET</strong>: returns the list of stocks purshaced by the user.

    @function <strong>POST</strong>: add one stock_app purchased by a user.<br/>
    <ul>
        <li>@param <strong>pricePayed</strong>: price paid for each stock_app</li>
        <li>@param <strong>quantityOwned</strong>: quantity of stocks purchased</li>
    </ul>
    """
    def get_object(self, user, stockId):
        return UserStock.objects.get(user__id=user.id, stock__id=stockId)

    def get(self, request, stockId):
        user = request.user
        userStock = self.get_object(user, stockId)
        serializer = UserStockPositionSerializer(userStock)
        return Response(serializer.data)

    @transaction.atomic
    def post(self, request, stockId):
        user = request.user

        response = Response(status=status.HTTP_400_BAD_REQUEST)
        if isinstance(request.data.get(PRICE_PAYED), (float, int)) and isinstance(request.data.get(QUANTITY_OWNED), int):
            userStock = UserStock()
            try:
                userStock = self.get_object(user, stockId)
            except UserStock.DoesNotExist:
                stock = Stock.objects.get(id=stockId)
                # answer = afiliateStockToUser(stock, request.user)
                answer = {}
                if answer.get(SUCCESS_TAG):
                    userStock = self.get_object(user, stockId)
                if answer.get(ERROR_TAG):
                    raise Http404
            userStock.pricePayed = request.data.get(PRICE_PAYED)
            userStock.datePurchased = long(time.time())
            userStock.quantityOwned = request.data.get(QUANTITY_OWNED)
            userStock.save(force_update=True)
            serializer = UserStockPositionSerializer(userStock)
            response = Response(serializer.data)
        return response
