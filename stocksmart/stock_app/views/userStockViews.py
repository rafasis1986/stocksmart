# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from django.http.response import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from stock_app.models.userStockModel import UserStock
from stock_app.serializers import StockExtendedSerializer
from stock_app.serializers.userStockSerializers import UserStockSerializer


class UserStockDetailView(APIView):
    """
    @precondition: To use all methods you must be authenticated and the stock_app pk indicate into url
    must be affiliated to user account logged.

    @function <strong>GET</strong>: returns the details from the account user and the affiliate stock_app.

    @function <strong>DELETE</strong>: disaffiliate the stock_app from the user account logged.
    """
    def get_object(self, user, stock_id):
        try:
            return UserStock.objects.get(user__id=user.id, stock__id=stock_id, stock__isIndex=False)
        except UserStock.DoesNotExist:
            raise Http404

    def get(self, request, stockId=None):
        user = request.user
        user_stock = self.get_object(user, stockId)
        serializer = UserStockSerializer(user_stock)
        return Response(serializer.data)

    def delete(self, request, stockId):
        user = request.user
        user_stock = self.get_object(user, stockId)
        user_stock.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserStocksView(APIView):
    """
    @precondition: To use all methods you must be authenticated.

    @function <strong>GET</strong>: returns the list of stocks affiliated to a user account
    <ul>
        <li>@param <strong>name</strong>:  name of stock_app</li>
    </ul>
    """
    def get(self, request):
        query = UserStock.objects.filter(user__id=request.user.id, stock__isIndex=False)
        stocks = list()
        for index in query:
            index.stock.pricePayed = index.pricePayed
            index.stock.quantityOwned = index.quantityOwned
            index.stock.datePurchased = index.datePurchased
            stocks.append(index.stock)
        serializer = StockExtendedSerializer(stocks, many=True)
        return Response(serializer.data)
