# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from django.db.models.query_utils import Q
from django.http.response import Http404
from rest_framework.response import Response
from rest_framework.views import APIView

from stock_app.models.stockHistoryModel import StockHistory
from stock_app.models.userStockModel import UserStock
from stock_app.serializers import StockHistorySerializer
from stock_app.utils.requestTags import DATE_FROM, DATE_TO


class StockHistoricalView(APIView):
    """
    @precondition: To use all methods you must be authenticated and the stock_app have been affiliated
    to account user previously.

    @function <strong>GET</strong>: return list with all historical info to stock_app from year 2005. <br>
    By default return the last 50 days history, but if you specified the date from and date to, will <br>
    return the information of historical in that period, the application has information from <br>
    2005-01-01, remember that labels for these arguments are <strong>dateFrom</strong> and <strong>dateTo</strong><br>
    and both are long type.<br>
    <br>
    Return arguments:
    <ul>
        <li>@param <strong>date</strong></li>
        <li>@param <strong>open</strong></li>
        <li>@param <strong>close</strong></li>
        <li>@param <strong>high</strong></li>
        <li>@param <strong>low</strong></li>
        <li>@param <strong>volume</strong></li>
    </ul>
    """
    def get_object(self, user, stockId):
        try:
            userStock = UserStock.objects.get(user__id=user.id, stock__id=stockId)
            return userStock
        except UserStock.DoesNotExist:
            raise Http404

    def get(self, request, stockId=None):
        user = request.user
        userStock = self.get_object(user, stockId)
        flag = True
        historical = []
        if not (request.REQUEST.get(DATE_FROM) is None or request.REQUEST.get(DATE_TO) is None):
            if request.REQUEST.get(DATE_FROM) < request.REQUEST.get(DATE_TO):
                dateFrom = long(request.REQUEST.get(DATE_FROM))
                dateTo = long(request.REQUEST.get(DATE_TO))
                historical = StockHistory.objects.filter(Q(stock=userStock.stock), Q(date__lte=dateTo),
                    Q(date__gte=dateFrom)).order_by('-date')
                flag = False
        if flag:
            historical = StockHistory.objects.filter(stock=userStock.stock).order_by('-date')[:50]
        serializer = StockHistorySerializer(historical, many=True)
        return Response(serializer.data)
