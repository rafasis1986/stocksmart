# -*- coding: utf-8 -*-
'''
Created on Aug 20, 2015

@author: rtorres
'''
from urllib2 import HTTPError

from celery.app import shared_task
from celery.utils.log import get_task_logger
from django.core.cache import cache
from googlefinance import getQuotes

from stock_app.models.stockModel import Stock
from stock_app.utils.constants import TIME_LONG_CACHE, VAR_CURRENT_PRICE_CACHE, \
    TIME_CURRENT_PRICE_CACHE, VAR_STOCKS_NAMES_CACHE, MAX_SYMBOLS, VAR_UPDATE_CURRENT_PRICE_CACHE, \
    TIME_SHORT_CACHE, LOCK_EXPIRE, VAR_YAHOO_NAMES_CACHE


logger = get_task_logger(__name__)


@shared_task
def updateCacheStocksSymbol():
    logger.info("Start updateCacheStocksSymbol()")
    stocks = Stock.objects.filter(isIndex=False).values('symbol', 'yahooSymbol').order_by('symbol')
    stockNames = list()
    yahooSymbols = list()
    for item in stocks:
        stockNames.append(str(item['symbol']))
        yahooSymbols.append(str(item['yahooSymbol']))
    cache.set(VAR_STOCKS_NAMES_CACHE, stockNames, TIME_LONG_CACHE)
    cache.set(VAR_YAHOO_NAMES_CACHE, yahooSymbols, TIME_LONG_CACHE)
    logger.info("End updateCacheStocksSymbol()")


@shared_task
def updateStocksCurrentPrice():
    logger.info("Start updateStocksCurrentPrice()")
    result = ''
    acquire_lock = lambda: cache.add(VAR_UPDATE_CURRENT_PRICE_CACHE, True, LOCK_EXPIRE)
    release_lock = lambda: cache.delete(VAR_UPDATE_CURRENT_PRICE_CACHE)
    if acquire_lock():
        try:
            cacheDict = dict()
            currentList = list()
            stockNames = cache.get(VAR_STOCKS_NAMES_CACHE, list())
            chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
            for items in chunks(stockNames, MAX_SYMBOLS):
                if len(stockNames) > 0:
                    currentList += getQuotes(items)
            for item in currentList:
                cacheDict.update({str(item['StockSymbol']): str(item['LastTradePrice'])})
            cache.set(VAR_CURRENT_PRICE_CACHE, cacheDict, TIME_CURRENT_PRICE_CACHE)
            result = cacheDict
        except HTTPError as e:
            logger.error("EXCEPTION! type:%s message:%s url:%s" % (type(e), e.msg, e.url))
            result = type(e)
        except Exception as e:
            logger.error("EXCEPTION! type:%s message:%s" % (type(e), e.message))
            result = type(e)
        finally:
            release_lock()
    logger.info("End updateStocksCurrentPrice()")
    return result


@shared_task
def saveLastPrice():
    logger.info("Start saveLastPrice()")
    if cache.get(VAR_CURRENT_PRICE_CACHE) is None:
        try:
            cache.set(VAR_UPDATE_CURRENT_PRICE_CACHE, False, TIME_SHORT_CACHE)
            cacheDict = cache.get(VAR_CURRENT_PRICE_CACHE)
            for item in cacheDict:
                try:
                    symbol = item.get('StockSymbol', '')
                    price = float(item.get('LastTradePrice', 0))
                    if not symbol == '':
                        Stock.objects.filter(symbol=symbol).update(lastTradePrice=price)
                except Exception as e:
                    logger.error("EXCEPTION! type:%s message:%s" % (type(e), e.message))
        except HTTPError as e:
            logger.error("EXCEPTION! type:%s message:%s url:%s" % (type(e), e.msg, e.url))
        except Exception as e:
            logger.error("EXCEPTION! type:%s message:%s" % (type(e), e.message))
    logger.info("End saveLastPrice()")
