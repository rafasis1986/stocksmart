# -*- coding: utf-8 -*-
'''
Created on Sep 11, 2015

@author: rtorres
'''
from threading import Thread

from celery.app import shared_task
from celery.utils.log import get_task_logger

from stock_app.utils.functions.news import addNews, updateNews
from stock_app.utils.constants import VAR_UPDATE_NEWS, LOCK_EXPIRE_LONG, VAR_GET_NEWS
from django.core.cache import cache


logger = get_task_logger(__name__)


@shared_task
def addStockNews():
    logger.info("Start addStockNews()")
    acquire_lock = lambda: cache.add(VAR_GET_NEWS, True, LOCK_EXPIRE_LONG)
    if acquire_lock():
        tAux = Thread(target=addNews, args=())
        tAux.start()
    logger.info("End addStockNews()")


@shared_task
def updateStockNews():
    logger.info("Start updateStockNews()")
    acquire_lock = lambda: cache.add(VAR_UPDATE_NEWS, True, LOCK_EXPIRE_LONG)
    if acquire_lock():
        tAux = Thread(target=updateNews, args=())
        tAux.start()
    logger.info("End updateStockNews()")
