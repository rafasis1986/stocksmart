# -*- coding: utf-8 -*-
'''
Created on Aug 20, 2015

@author: rtorres
'''
from celery.app import shared_task
from celery.utils.log import get_task_logger

from stock_app.utils.functions.stadistics import updateAllSotckStatistics, updateAllClosePrice, \
    updateAllOpenPrice


logger = get_task_logger(__name__)


@shared_task
def updateStocksStatistics():
    logger.info("Start updateStocksStatistics()")
    result = False
    try:
        updateAllSotckStatistics()
        result = True
    except Exception as e:
        logger.error("Task Exception: message = %s  " % e.message)
        result = type(e)
    logger.info("End updateStocksStatistics()")
    return result


@shared_task
def updateStocksClosePrice():
    logger.info("Start updateStocksClosePrice()")
    result = False
    try:
        updateAllClosePrice()
        result = True
    except Exception as e:
        logger.error("Task Exception: message = %s  " % e.message)
        result = type(e)
    logger.info("End updateStocksClosePrice()")
    return result


@shared_task
def updateStocksOpenPrice():
    logger.info("Start updateStocksOpenPrice()")
    result = False
    try:
        updateAllOpenPrice()
        result = True
    except Exception as e:
        logger.error("Task Exception: message = %s  " % e.message)
        result = type(e)
    logger.info("End updateStocksOpenPrice()")
    return result
