# -*- coding: utf-8 -*-
'''
Created on Aug 20, 2015

@author: rtorres
'''
from datetime import date, timedelta

from celery.app import shared_task
from celery.utils.log import get_task_logger
from django.core.cache import cache

from stock_app.models.stockModel import Stock
from stock_app.utils.classes.threadWithReturnValue import ThreadWithReturnValue
from stock_app.utils.constants import VAR_HISTORY_CACHE, TIME_LONG_CACHE, MAX_THREADS, BEGIN_YEAR
from stock_app.utils.functions.historicals import addHistoricalToStockByYears, addHistoricalToStock, \
    setAllPreviousDayClose


logger = get_task_logger(__name__)


@shared_task
def addInitialHistoricalStock():
    logger.info("Start addInitialHistoricalStock()")
    if cache.get(VAR_HISTORY_CACHE) in (True, None):
        try:
            cache.set(VAR_HISTORY_CACHE, False, TIME_LONG_CACHE)
            today = date.today()
            stocksList = Stock.objects.filter(hasHistory=False).order_by('id')
            # chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
            # for stocks in chunks(stocksList, MAX_THREADS):
            for stock in stocksList:
                try:
                    addHistoricalToStockByYears(stock.id, BEGIN_YEAR, today.year)
                    stock.hasHistory = True
                    stock.hasRecentHistory = True
                    stock.save()
                except Exception as e:
                    logger.critical("Stock: %s, exception: %s, message: %s" % (stock.id, e.__class__.__name__, e.message))
        except Exception as e:
            logger.critical("addInitialHistoricalStock() exception: %s, message: %s" % (e.__class__.__name__, e.message))
        finally:
            cache.set(VAR_HISTORY_CACHE, True, TIME_LONG_CACHE)
    logger.info("End addInitialHistoricalStock()")


@shared_task
def updateHistoricalStock():
    logger.info("Start updateHistoricalStock()")
    try:
        prevStocks = Stock.objects.filter(hasHistory=True).order_by('id')
        today = date.today()
        endDate = today
        initDate = today - timedelta(days=1)
        threads = list()
        chunks = lambda l, n: [l[x: x + n] for x in xrange(0, len(l), n)]
        for stocks in chunks(prevStocks, MAX_THREADS):
            for stock in stocks:
                tAux = ThreadWithReturnValue(target=addHistoricalToStock, args=(stock, initDate, endDate))
                threads.append(tAux)
            for t in threads:
                try:
                    t.start()
                except Exception as e:
                    logger.critical('thread exception: %s, message: %s' % (type(e), e.message))
            for t in threads:
                t.join()
        setAllPreviousDayClose()
    except Exception as e:
        logger.critical('updateHistoricalStock exception: %s, message: %s' % (type(e), e.message))
    logger.info("End updateHistoricalStock()")
