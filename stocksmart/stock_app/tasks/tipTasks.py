# -*- coding: utf-8 -*-
'''
Created on Aug 20, 2015

@author: rtorres
'''
from celery.app import shared_task
from celery.utils.log import get_task_logger
from django.core.cache import cache

from stock_app.models.stockTipsModel import StockTipsModel
from stock_app.utils.constants import STATUS_WAITING
from stock_app.utils.functions.tips import sendStockTip


logger = get_task_logger(__name__)
LOCK_EXPIRE = 60
LOCK_ID = 'TIPS_LOCK'


@shared_task
def sendStockTipsTask():
    acquire_lock = lambda: cache.add(LOCK_ID, True, LOCK_EXPIRE)
    release_lock = lambda: cache.delete(LOCK_ID)
    logger.debug("Start sendStockTip()")
    if acquire_lock():
        try:
            tipsObjects = StockTipsModel.objects.filter(status=STATUS_WAITING)
            for tip in tipsObjects:
                sendStockTip(tip.pk)
        finally:
            release_lock()
    logger.info("End sendStockTip()")
