# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''
from rest_framework import serializers

from stock_app.models.stockNewsModel import StockNewsModel


class StockNewsSerializer(serializers.ModelSerializer):

    class Meta():
        model = StockNewsModel
        fields = ('id', 'title', 'desc', 'pubDate', 'link')
