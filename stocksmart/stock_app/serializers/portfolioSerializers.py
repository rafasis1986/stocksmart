# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''
from rest_framework import serializers

from stock_app.models.portfolioModel import Portfolio
from stock_app.serializers.stockSerializers import StockSerializer


class PortfolioSerializer(serializers.ModelSerializer):
    stocks = StockSerializer(many=True, read_only=True)

    class Meta:
        model = Portfolio
        fields = ('id', 'name', 'owner', 'stocks')


class PortfolioExtendedSerializer(PortfolioSerializer):

    #===============================================================================================
    # def stocksPayed(self, portfoliomodel):
    #     return self.stocksEx
    #===============================================================================================

    class Meta(PortfolioSerializer.Meta):
        fields = ('id', 'name', 'owner', 'stocks')

    #===============================================================================================
    # def __init__(self, instance=None, data=empty, **kwargs):
    #     self.stocksEx = data.get(STOCKS_EXTENDED, list())
    #     super(PortfolioExtendedSerializer, self).__init__(instance=instance, data=empty, **kwargs)
    #===============================================================================================
