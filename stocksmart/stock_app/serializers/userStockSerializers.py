# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from rest_framework import serializers

from login.serializers import UserSerializer
from stock_app.models.userStockModel import UserStock
from stock_app.serializers import StockSerializer


class UserStockSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    stock = StockSerializer()

    class Meta:
        model = UserStock
        fields = ('user', 'stock', 'dateAfiliated', 'pricePayed', 'quantityOwned', 'datePurchased')


class UserStockPositionSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserStock
        fields = ('pricePayed', 'quantityOwned', 'datePurchased')
