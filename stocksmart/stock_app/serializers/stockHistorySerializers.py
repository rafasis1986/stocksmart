# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from rest_framework import serializers

from stock_app.models.stockHistoryModel import StockHistory


class StockHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = StockHistory
        fields = ('date', 'open', 'close', 'high', 'low', 'volume')
