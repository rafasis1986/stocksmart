# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''
from django.db.models import Q
from rest_framework import serializers

from login.serializers import UserSerializer
from stock_app.models.stockTipsModel import StockTipsModel
from stock_app.serializers.stockSerializers import StockSerializer


class OwnerSerializer(UserSerializer):

    class Meta(UserSerializer.Meta):
        fields = ('id', 'username', 'email', 'phone')


class StockTipSerializer(serializers.ModelSerializer):
    stock = StockSerializer()
    owner = OwnerSerializer()
    sentiments = serializers.SerializerMethodField('sentimentsTip')

    def sentimentsTip(self, stocktipsmodel):
        sentiments = stocktipsmodel.usertipmodel_set.filter(~Q(sentiment=None)).\
            extra(select={'userId': 'user_id'}).values('userId', 'sentiment').order_by('userId')
        return sentiments

    class Meta():
        model = StockTipsModel
        fields = ('id', 'owner', 'type', 'owner', 'dateCreated', 'dateSent', 'tipPrice', 'stock',
                  'performance', 'performanceInPercentage', 'message', 'sentiments')


class StockTipSentSerializer(StockTipSerializer):
    contacts = serializers.SerializerMethodField('contacsTip')

    def contacsTip(self, stocktipsmodel):
        contacts = stocktipsmodel.contactmodel_set.all().values('name', 'phone', 'status').\
            order_by('name')
        return [item for item in contacts]

    class Meta(StockTipSerializer.Meta):
        fields = ('id', 'status', 'type', 'owner', 'dateCreated', 'dateSent', 'tipPrice', 'stock',
                  'performance', 'performanceInPercentage', 'message', 'contacts', 'sentiments')


class StockTipReceivedSerializer(StockTipSerializer):

    class Meta(StockTipSerializer.Meta):
        fields = ('id', 'owner', 'type', 'owner', 'dateCreated', 'dateSent', 'tipPrice', 'stock',
                  'performance', 'performanceInPercentage', 'message', 'sentiments')
