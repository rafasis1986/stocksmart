# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from rest_framework import serializers

from stock_app.models.indexModel import IndexModel


class IndexSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('nameStock')
    currentPrice = serializers.SerializerMethodField('currentPriceStock')
    closePrice = serializers.SerializerMethodField('closePriceStock')
    openPrice = serializers.SerializerMethodField('openPriceStock')
    dateLastConsulted = serializers.SerializerMethodField('dateLastConsultedStock')

    def nameStock(self, indexmodel):
        return indexmodel.stock.name

    def currentPriceStock(self, indexmodel):
        return indexmodel.stock.currentPrice

    def openPriceStock(self, indexmodel):
        return indexmodel.stock.openPrice

    def closePriceStock(self, indexmodel):
        return indexmodel.stock.closePrice

    def dateLastConsultedStock(self, indexmodel):
        return indexmodel.stock.dateLastConsulted

    class Meta:
        model = IndexModel
        fields = ('id', 'name', 'yahooSymbol', 'googleSymbol', 'currentPrice',
                  'openPrice', 'closePrice', 'dateLastConsulted')
