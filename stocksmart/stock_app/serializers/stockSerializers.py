# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from rest_framework import serializers

from stock_app.models.stockModel import Stock


class StockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stock
        fields = ('id', 'name', 'symbol', 'yahooSymbol', 'index', 'compositeName', 'sector',
                  'industry', 'openPrice', 'currentPrice', 'fiftyDayAvg', 'twoHundredDayAvg',
                  'previousDayClose', 'yearHigh', 'yearLow', 'nextEarning', 'eps', 'pe',
                  'hasHistory', 'hasRecentHistory')


class StockExtendedSerializer(serializers.ModelSerializer):
    pricePayed = serializers.CharField()
    quantityOwned = serializers.IntegerField()
    datePurchased = serializers.CharField()

    class Meta(StockSerializer.Meta):
        fields = ('id', 'name', 'symbol', 'yahooSymbol', 'index', 'compositeName', 'sector', 'industry',
                  'compositeName', 'openPrice', 'currentPrice', 'fiftyDayAvg', 'twoHundredDayAvg',
                  'previousDayClose', 'yearHigh', 'yearLow', 'pricePayed', 'quantityOwned',
                  'datePurchased', 'nextEarning', 'eps', 'pe', 'hasHistory', 'hasRecentHistory')


class StockBasicSerializer(StockSerializer):

    class Meta(StockSerializer.Meta):
        fields = ('id', 'name', 'symbol')
