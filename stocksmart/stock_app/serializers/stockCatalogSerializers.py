# -*- coding: utf-8 -*-
'''
Created on Aug 31, 2015

@author: rtorres
'''
from rest_framework import serializers

from stock_app.models.stockCatalogModel import StockCatalogModel


class StockCatalogSerializer(serializers.ModelSerializer):

    class Meta:
        model = StockCatalogModel
        fields = ('symbol', 'name')
