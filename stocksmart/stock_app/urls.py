# -*- coding: utf-8 -*-

from django.conf.urls import url

from config.utils.constants import VERSION_API
from stock_app.views.indexViews import IndexesView, IndexDetailView, IndexesHistoricalView
from stock_app.viewsets.featuredPortfolioViewSet import FeaturedPortfolioViewSet
from stock_app.viewsets.portfolioViewSet import PortfolioViewSet
from stock_app.viewsets.stockCatalogViewSet import StockCatalogViewSet
from stock_app.viewsets.stockNewsViewSet import StockNewsViewSet
from stock_app.viewsets.stockTipViewSet import StockTipViewSet


PREFIX_URL = VERSION_API + '/'

portfolioIdView = PortfolioViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', })
portfolioList = PortfolioViewSet.as_view({'get': 'list', 'post': 'create'})
portfolioStocks = PortfolioViewSet.as_view({'post': 'update'})
portfolioStocksRemove = PortfolioViewSet.as_view({'delete': 'remove'})
catalogList = StockCatalogViewSet.as_view({'get': 'list'})
stockTipsView = StockTipViewSet.as_view({'post': 'create', 'get': 'list'})
postSentimentTip = StockTipViewSet.as_view({'post': 'sentiment'})
listFeaturedView = FeaturedPortfolioViewSet.as_view({'get': 'list'})
getFeaturedView = FeaturedPortfolioViewSet.as_view({'get': 'retrieve'})
listNewsView = StockNewsViewSet.as_view({'get': 'list'})

stock_patterns = [
    url(PREFIX_URL + 'indexes$', IndexesView.as_view()),
    url(PREFIX_URL + 'indexes/(?P<indexId>[a-zA-Z0-9_]+)$', IndexDetailView.as_view()),
    url(PREFIX_URL + 'indexes/(?P<indexId>[a-zA-Z0-9_]+)/history$', IndexesHistoricalView.as_view()),
    url(PREFIX_URL + 'portfolios/featured$', listFeaturedView),
    url(PREFIX_URL + 'portfolios/featured/(?P<pk>[a-zA-Z0-9_]+)$', getFeaturedView),
    url(PREFIX_URL + 'stocks/(?P<stockId>[a-zA-Z0-9_]+)/news$', listNewsView),
    url(PREFIX_URL + 'symbols/search$', catalogList),
    url(PREFIX_URL + 'users/me/portfolios$', portfolioList),
    url(PREFIX_URL + 'users/me/portfolios/(?P<pk>[a-zA-Z0-9_]+)$', portfolioIdView),
    url(PREFIX_URL + 'users/me/portfolios/(?P<pk>[a-zA-Z0-9_]+)/stocks$', portfolioStocks),
    url(PREFIX_URL + 'users/me/portfolios/(?P<pk>[a-zA-Z0-9_]+)/stocks/(?P<symbol>[\w-]+)$', portfolioStocksRemove),
    url(PREFIX_URL + 'users/me/tips$', stockTipsView),
    url(PREFIX_URL + 'users/me/tips/(?P<tipId>[a-zA-Z0-9_]+)/sentiment$', postSentimentTip),
]
