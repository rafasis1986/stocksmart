"""Development settings and globals."""

from .base import *
from .configLocal import config


########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
TEMPLATE_DEBUG = DEBUG
########## END DEBUG CONFIGURATION


########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
########## END EMAIL CONFIGURATION


########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': config()
}
########## END DATABASE CONFIGURATION


########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'localhost:6379',
        # 'LOCATION': '/var/run/redis/redis.sock',
    },
}
########## END CACHE CONFIGURATION


######### TOOLBAR CONFIGURATION
# See: http://django-debug-toolbar.readthedocs.org/en/latest/installation.html#explicit-setup
INSTALLED_APPS += (
     'debug_toolbar',
)

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

#===================================================================================================
# DEBUG_TOOLBAR_PATCH_SETTINGS = False
#===================================================================================================

# http://django-debug-toolbar.readthedocs.org/en/latest/installation.html
INTERNAL_IPS = ('127.0.0.1',)
########## END TOOLBAR CONFIGURATION

# CELERY_RESULT_BACKEND = 'redis://'
# BROKER_URL = "amqp://stockbeat:ST0CKB34T@localhost:5672/stockbeat"  # host where RabbitMQ is running
BROKER_URL = "amqp://guest@localhost:5672//"  # host where RabbitMQ is running
# BROKER_URL = 'redis+socket:///var/run/redis/redis.sock'
TWILIO_ACCOUNT_SID = "AC0ad3813a644ee7083fab4e40766c701e"
TWILIO_AUTH_TOKEN = "3dc30c519903dfc1c9ed0b643dead75d"
TWILIO_ENVIROMENT_TEST = False
TWILIO_PHONE_NUMBER = "+17147257359"
TWILIO_PHONE_TESTING = None

PUSH_NOTIFICATIONS_SETTINGS = {
    "APNS_CERTIFICATE": BINARY_ROOT + '/stockbeat.pem',
}
