"""Production settings and globals."""
from os import environ


from .base import *
from .configProduction import config



########## HOST CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/releases/1.5/#allowed-hosts-required-in-production
ALLOWED_HOSTS = ['*']
########## END HOST CONFIGURATION

########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = environ.get('EMAIL_HOST', 'smtp.gmail.com')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-password
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', '')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-user
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER', 'your_email@example.com')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = environ.get('EMAIL_PORT', 587)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-use-tls
EMAIL_USE_TLS = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = EMAIL_HOST_USER
########## END EMAIL CONFIGURATION

########## DATABASE CONFIGURATION
DATABASES = {
    'default': config()
}
########## END DATABASE CONFIGURATION
"""
MIDDLEWARE_CLASSES = ('django.middleware.cache.UpdateCacheMiddleware', ) + MIDDLEWARE_CLASSES + \
                    ('django.middleware.cache.FetchFromCacheMiddleware', )
"""
########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '/tmp/redis.sock',
    },
}

#BROKER_URL = 'redis+socket:///tmp/redis.sock'
BROKER_URL = "amqp://stockbeat:ST0CKB34T@localhost:5672/stockbeat"
########## END CACHE CONFIGURATION

# SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
TWILIO_ACCOUNT_SID = "ACfc79ac3e77ab6eb572c726724a443052"
TWILIO_AUTH_TOKEN = "290692344a60f70fbec82cd88757337e"
TWILIO_ENVIROMENT_TEST = False
TWILIO_PHONE_NUMBER = "+13478265688"
TWILIO_PHONE_TESTING = None

PUSH_NOTIFICATIONS_SETTINGS = {
    "APNS_CERTIFICATE": BINARY_ROOT + '/stockbeat.pem',
    "APNS_HOST": 'gateway.sandbox.push.apple.com',
}
