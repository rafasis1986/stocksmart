# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''

from datetime import timedelta

from celery.schedules import crontab
from kombu.entity import Exchange, Queue


def celeryQueues():
    return (Queue('default', routing_key='task.#'),
            Queue('news', routing_key='news.#'),
            )


def pathTasks():
    return ("stock_app.tasks.stadisticStockTasks",
            "stock_app.tasks.historicalTasks",
            "stock_app.tasks.currentStockValueTasks",
            "stock_app.tasks.tipTasks",
            "stock_app.tasks.newsTasks",)


def scheduleTasks():
    return {
        'task-open-time': {
            'task': 'stock_app.tasks.stadisticStockTasks.updateStocksStatistics',
            'schedule': crontab(hour='9', minute='5,10,20'),
        },
        'task-update-close-price': {
            'task': 'stock_app.tasks.stadisticStockTasks.updateStocksClosePrice',
            'schedule': crontab(hour='16', minute='30,40,50'),
        },
        'task-update-open-price': {
            'task': 'stock_app.tasks.stadisticStockTasks.updateStocksOpenPrice',
            'schedule': crontab(hour='9', minute='43,58'),
        },
        'task-add-historical-stock': {
            'task': 'stock_app.tasks.historicalTasks.addInitialHistoricalStock',
            'schedule': timedelta(minutes=10),
        },
        'task-update-historical-stock': {
            'task': 'stock_app.tasks.historicalTasks.updateHistoricalStock',
            'schedule': crontab(hour='8', minute='8,15'),
        },
        'task-update-symbols': {
            'task': 'stock_app.tasks.currentStockValueTasks.updateCacheStocksSymbol',
            'schedule': crontab(minute='*/1', hour='9-16', day_of_week='mon-fri'),
        },
        'task-update-current-price': {
            'task': 'stock_app.tasks.currentStockValueTasks.updateStocksCurrentPrice',
            'schedule': crontab(minute='*/2', hour='9-16', day_of_week='mon-fri'),
            # changed to develop enviroment
            # 'schedule': crontab(minute='*/4', hour='9-16', day_of_week='mon-fri'),
        },
        'task-update-send-tips': {
            'task': 'stock_app.tasks.tipTasks.sendStockTipsTask',
            'schedule': crontab(minute='*/1'),
            # changed to develop enviroment
            # 'schedule': crontab(minute='*/2'),
        },
        'task-update-trade-price': {
            'task': 'stock_app.tasks.currentStockValueTasks.saveLastPrice',
            'schedule': crontab(minute='*/15', hour='9-16', day_of_week='mon-fri'),
        },
        'task-add-news-stock': {
            'task': 'stock_app.tasks.newsTasks.addStockNews',
            'schedule': crontab(minute='*/2'),
            'queue': 'news',
            'routing_key': 'news.stock',
        },
        'task-update-news-stock': {
            'task': 'stock_app.tasks.newsTasks.updateStockNews',
            'schedule': crontab(minute='*/5'),
            'queue': 'news',
            'routing_key': 'news.stock',
        },
    }
