# -*- coding: utf-8 -*-
'''
Created on Aug 24, 2015

@author: rtorres
'''


def config():
    config = {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
    return config
