# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''
from simplejson.encoder import JSONEncoder


class FinanceJsonEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__
