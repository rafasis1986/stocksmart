# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''
from config.utils.errorMessages import MSG_DEFAULT_PROJECT_ERROR, MSG_TECHNICAL_ERROR, \
    MSG_LOGICAL_ERROR


class GenericErrorResponse(object):
    code = None
    message = None
    error = None

    def __init__(self, message=None):
        self.error = self.__class__.__name__
        self.code = 0
        self.message = MSG_DEFAULT_PROJECT_ERROR if message is None else message


class TechnichalErrorResponse(GenericErrorResponse):

    def __init__(self, message=None):
        super(TechnichalErrorResponse, self).__init__(MSG_TECHNICAL_ERROR if message is None else message)
        self.code = 1000
        self.error = self.__class__.__name__


class LogicalErrorResponse(GenericErrorResponse):

    def __init__(self, message=None):
        super(LogicalErrorResponse, self).__init__(MSG_LOGICAL_ERROR if message is None else message)
        self.code = 2000
        self.error = self.__class__.__name__
