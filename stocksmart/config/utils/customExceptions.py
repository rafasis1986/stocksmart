# -*- coding: utf-8 -*-
'''
Created on Aug 10, 2015

@author: rtorres
'''


class SerializerException(Exception):

    def __init__(self, argument=None):
        self.message = 'Serializer object error' if argument is None else argument
