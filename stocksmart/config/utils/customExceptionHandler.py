# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''

import logging

from rest_framework.views import exception_handler


logger = logging.getLogger(__name__)


def handlerException(exc, context):
    '''
    Call REST framework's default exception handler first,
    to get the standard error response.
    '''
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['code'] = response.status_code
        response.data['message'] = response.data.get('detail', '')
        if response.data['message'] != '':
            del response.data['detail']
        logger.error('%s, %s', response.status_code, response.data['message'])
    return response
