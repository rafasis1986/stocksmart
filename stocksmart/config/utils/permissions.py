'''
Created on 9/4/2015

@author: rtorres
'''
from encodings.base64_codec import base64_encode

from rest_framework.permissions import BasePermission

from config.settings.base import APP_TOKEN


class IsPostRequest(BasePermission):

    def has_permission(self, request, view):
        return request.method == "POST"


class IsGetRequest(BasePermission):

    def has_permission(self, request, view):
        return request.method == "GET"


class IsAppRequest(BasePermission):

    def has_permission(self, request, view):
        import re
        response = False
        regex = re.compile('^HTTP_')
        headers = dict((regex.sub('', header), value) for (header, value)
           in request.META.items() if header.startswith('HTTP_'))
        if headers.get('APP_AUTHORIZATION') and headers.get('APP_AUTHORIZATION').find('Bearer') != -1:
            tokens = headers.get('APP_AUTHORIZATION').split(' ')
            if len(tokens) == 2:
                response = APP_TOKEN == base64_encode(tokens[1])
        return response
