import string, random


def generateKey(length=8):
    '''
    function to generate alphanumeric hash Key with probability to
    repeat one arround 1 / (1,56 * 10^12)
    '''
    ret = []
    for i in range(0, length):
        if i % 3 == 0:
            ret.extend(random.sample(string.hexdigits, 1))
        else:
            ret.extend(random.sample(string.letters, 1))
    return ''.join(ret)


def generateDigitsKey(length=8):
    '''
    function to generate alphanumeric hash Key with probability to
    repeat one arround 1 / (1,56 * 10^12)
    '''
    ret = []
    ret.extend(random.sample(string.digits, length))
    return ''.join(ret)
