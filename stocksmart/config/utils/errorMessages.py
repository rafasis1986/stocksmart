# -*- coding: utf-8 -*-
'''
Created on 14/4/2015

@author: rtorres
'''
CODE_ERROR_PROJECT = 'F'


MSG_DEFAULT_PROJECT_ERROR = 'default project error'
MSG_DUPICATE_PORTFOLIO = "duplicate portfolio"
MSG_TECHNICAL_ERROR = 'technical error'
MSG_LOGICAL_ERROR = 'logical error'
