from django.conf import settings
from django.conf.urls import include, url, patterns
from django.contrib import admin

from login_stockbeat.urls import login_patterns
from stock_app.urls import stock_patterns


urlpatterns = [
    # Examples:
    # url(r'^$', 'config.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
] + stock_patterns + login_patterns


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
