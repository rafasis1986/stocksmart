# -*- coding: utf-8 -*-
'''
Created on Aug 10, 2015

@author: rtorres
'''
from django.db import models

from config.utils.functions import generateKey


class AbstractHashModel(models.Model):
    id = models.SlugField(primary_key=True, unique=True, editable=False)

    def save(self, *args, **kwargs):
        while not self.id:
            newslug = generateKey()
            if type(self).objects.filter(id=newslug).count():
                newslug = generateKey()
            self.id = newslug

    class Meta:
        abstract = True
